using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(InstrumentTicket))]
public class EditorInstrumentTicket : Editor
{

    SerializedProperty id;

    public void Awake()
    {
        id = serializedObject.FindProperty("_id");
    }

}