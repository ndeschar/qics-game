using System.Collections.Generic;
using UnityEngine;

//TEMP Public
public enum ConnectorType
{
    Emitter,
    Receptor,
    Both
}

public class Connector : MonoBehaviour, ITicket, INeedThreadManager
{
    public IDraggable spawnedObject
    {
        get;
        set;
    }
    public Instrument AssociatedInstrument { get; private set; }

    public List<ClassicalThread> classicalThreads;

    private LevelManager _levelManager;
    private IClassicalReceptor _receptorInterface;
    private IClassicalEmitter _emitterInterface;
    private IThreadManager _threadManager;
    public IThreadManager ThreadMana
    {
        get
        {
            if (_threadManager == null)
                _levelManager.QueryThreadManager(this);
            return (_threadManager);
        }
    }
    //TEMP Public
    public ConnectorType Type { get; private set; }

    public bool _isEmiter = false;

    public void Awake()
    {
        _levelManager = LevelManager.Instance;
        _levelManager.QueryThreadManager(this);
        AssociatedInstrument = GetComponentInParent<Instrument>();
        _receptorInterface = AssociatedInstrument.GetComponent<IClassicalReceptor>();
        _emitterInterface = AssociatedInstrument.GetComponent<IClassicalEmitter>();
        if (_receptorInterface != null && _emitterInterface != null)
            Type = ConnectorType.Both;
        if (_receptorInterface != null)
            Type = ConnectorType.Receptor;
        if (_emitterInterface != null)
            Type = ConnectorType.Emitter;
    }

    public Transform InstantiateDraggableObject(Vector3 position)
    {
        ClassicalThread createdThread;


        createdThread = ThreadMana.InstantiateThread(position);
        createdThread.SetConnectorOne(this);
        /*if (_isEmiter)
            createdThread.connectorEmitter = this;
        else
            createdThread.connectorReceptor = this;*/

        return (createdThread.transform);
    }

    public void SendSignal(bool signalToSend)
    {
        foreach (ClassicalThread thread in classicalThreads)
        {
            thread.TransportClassicalInformation(signalToSend, this);
        }
    }

    public void ReceiveSignal(bool signalToReceive)
    {
        _receptorInterface.ReceiveSignal(signalToReceive);
    }

    public void AddThread(ClassicalThread thread)
    {
        classicalThreads.Add(thread);
        if (Type == ConnectorType.Receptor && classicalThreads.Count == 1)
        {
            _receptorInterface.SetIsControlled(true);
        }
    }

    public void RemoveThread(ClassicalThread thread)
    {
        classicalThreads.Remove(thread);
        if (Type == ConnectorType.Receptor && classicalThreads.Count == 0)
        {
            _receptorInterface.SetIsControlled(false);
        }
    }

    public static bool IsConnectedOrSameType(Connector connectOne, Connector connectTwo)
    {
        if (connectOne.Type == connectTwo.Type && connectOne.Type != ConnectorType.Both)
            return (true);
        foreach (ClassicalThread thread in connectOne.classicalThreads)
        {
            if (thread.ConnectorTwo == connectTwo)
                return (true);
        }
        return (false);
    }

    public void SetThreadManager(IThreadManager manager)
    {
        _threadManager = manager;
    }
}
