using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeamSplitter : Instrument, ITriggerCycle, ITriggerClear
{
    //private List<string> _createdModesName;
    private SpatialMode[] _inputModes;
    private bool _isTriggered = false;
    private List<SpatialMode> _createdModes;
    //private SpatialMode _lastCreatedMode;

    public override void InitInstrument()
    {
        //_createdModesName = new List<string>();
        _inputModes = new SpatialMode[4];
        _createdModes = new List<SpatialMode>();

        //_lastCreatedMode = null;
        for (int i = 0; i < 4; i++)
            _inputModes[i] = null;
        SubscribeCycle();
        SubscribeClear();
    }

    public void TriggerCycle()
    {
        UnsubscribeCycle();
        for (int i = 0; i < 4; i++)
        {
            if (_inputModes[i] && _inputModes[i].isDestroyed == true)
                _inputModes[i] = null;
        }

        //As a way to generalize the behaviour of the BeamSplitter, could have done it differently with a list or an array,
        //however, i'm not sure it would have been "better", here objects are clearly defined. We loose a bit of optimization but I would say it's not relevant.
        //Readability first

        Orientation currentUp = m_orientation.NextClockwiseOrientation();
        Orientation currentRight = currentUp.NextClockwiseOrientation();
        Orientation currentDown = currentRight.NextClockwiseOrientation();

        if (_inputModes[(int)currentUp] && _inputModes[(int)currentRight])
            TwoMode(_inputModes[(int)currentRight], _inputModes[(int)currentUp]);
        else if (_inputModes[(int)currentUp])
            OneMode(_inputModes[(int)currentUp], -1, currentRight);
        else if (_inputModes[(int)currentRight])
            OneMode(_inputModes[(int)currentRight], 1, currentUp);

        if (_inputModes[(int)m_orientation] && _inputModes[(int)currentDown])
            TwoMode(_inputModes[(int)currentDown], _inputModes[(int)m_orientation]);
        else if (_inputModes[(int)m_orientation])
            OneMode(_inputModes[(int)m_orientation], -1, currentDown);
        else if (_inputModes[(int)currentDown])
            OneMode(_inputModes[(int)currentDown], 1, m_orientation);
        for (int i = 0; i < 4; i++)
            _inputModes[i] = null;
        _isTriggered = false;

    }

    public int modeId = 0;

    public void OneMode(SpatialMode mode, int phase, Orientation createdModeOrientation)
    {
        Faketon owner = mode.owner;
        SpatialMode createdMode;
        List<State> newListState = new List<State>();

        //POSSIBLE SOURCE OF FUTURE BUG
        _createdModes.Add(GameObject.Instantiate(mode.gameObject, _levelManager.levelGrid.transform).GetComponent<SpatialMode>());
        
        createdMode = _createdModes[_createdModes.Count - 1];
        createdMode.AddInstrument(this);
        createdMode.Unsubscribe();
        //Shoud change this at some point
        createdMode.name = "test_" + modeId.ToString();
        createdMode.transform.SetParent(owner.transform);
        modeId += 1;
        createdMode.orientation = createdModeOrientation;
        //owner.Print();
        owner.numberOfSpatialModes += 1;
        owner.spatialModes.Add(createdMode);
        int indexOfMode = owner.spatialModes.IndexOf(mode);
        foreach (State state in owner.stateList)
        {
            int numberOfQubit = state.numberOfQubits[indexOfMode];
            for (int j = 0; j <= numberOfQubit; j++)
            {
                State newState = new State();
                newState.proba = state.proba * Newton(j, numberOfQubit) / Mathf.Pow(2, numberOfQubit);
                newState.numberOfQubits = new int[owner.numberOfSpatialModes];
                for (int k = 0; k < owner.numberOfSpatialModes - 1; k++)
                {
                    newState.numberOfQubits[k] = state.numberOfQubits[k];
                }
                newState.numberOfQubits[indexOfMode] = j;
                newState.numberOfQubits[owner.numberOfSpatialModes - 1] = numberOfQubit - j;
                newState.phase = state.phase * (int)Mathf.Pow(phase, numberOfQubit - j);
                newListState.Add(newState);


            }
        }

        owner.stateList = Faketon.NormalizeLinearCombination(newListState);

        owner.DrawFaketon();
        mode.TriggerCycle();
        createdMode.TriggerCycle();
        mode.Subscribe();
        createdMode.Subscribe();
        if (owner.stateList.Count > _levelManager.MaxLenghtOfLinearCombination)
           	_levelManager.Error(mode.transform.position, ErrorType.TooMuchStates);
        if (owner.numberOfSpatialModes > _levelManager.MaxNumberOfSpatialModes)
            _levelManager.Error(mode.transform.position, ErrorType.TooMuchModes);
    }

    public void TwoMode(SpatialMode modeReflectedPositively, SpatialMode modeReflectedNegatively)
    {
        Faketon owner1 = modeReflectedPositively.owner;
        Faketon owner2 = modeReflectedNegatively.owner;
        float maxProba = 0f;
        List<State> newListState = new List<State>();

        if (owner1 == owner2)
        {
            int indexOfMode1 = owner1.spatialModes.IndexOf(modeReflectedPositively);
            int indexOfMode2 = owner1.spatialModes.IndexOf(modeReflectedNegatively);

            //owner1.Print();
            foreach (State state in owner1.stateList)
            {
                int numberOfQubit1 = state.numberOfQubits[indexOfMode1];
                int numberOfQubit2 = state.numberOfQubits[indexOfMode2];
                int normalizationCreationOperators = Fact(numberOfQubit1, 1) * Fact(numberOfQubit2, 1);

                for (int j = 0; j <= numberOfQubit1; j++)
                {
                    for (int l = 0; l <= numberOfQubit2; l++)
                    {
                        State newState = new State();


                        newState.numberOfQubits = new int[owner1.numberOfSpatialModes];
                        for (int k = 0; k < owner1.numberOfSpatialModes; k++)
                        {
                            if (k != indexOfMode1 && k != indexOfMode2)
                            {
                                newState.numberOfQubits[k] = state.numberOfQubits[k];
                            }

                        }
                        newState.numberOfQubits[indexOfMode1] = numberOfQubit1 + numberOfQubit2 - j - l;
                        newState.numberOfQubits[indexOfMode2] = j + l;
                        newState.proba = state.proba * Mathf.Pow(Newton(j, numberOfQubit1) * Newton(l, numberOfQubit2), 2) / Mathf.Pow(2, (numberOfQubit1 + numberOfQubit2)) * Fact(numberOfQubit1 + numberOfQubit2 - j - l, 1) * Fact(j + l, 1) / normalizationCreationOperators;
                        newState.phase = state.phase * (int)Mathf.Pow(-1, numberOfQubit2 - l);
                        int prevIndex = Faketon.GetIndexOfState(newListState, newState.numberOfQubits);
                        if (prevIndex >= 0)
                        {
                            State prevState = newListState[prevIndex];
                            maxProba -= prevState.proba;
                            if (prevState.phase == newState.phase)
                                prevState.proba = Mathf.Pow(Mathf.Sqrt(prevState.proba) + Mathf.Sqrt(newState.proba), 2);
                            else
                            {
                                if (prevState.proba > newState.proba)
                                    prevState.proba = Mathf.Pow(Mathf.Sqrt(prevState.proba) - Mathf.Sqrt(newState.proba), 2);
                                else
                                {
                                    prevState.phase = newState.phase;
                                    prevState.proba = Mathf.Pow(-Mathf.Sqrt(prevState.proba) + Mathf.Sqrt(newState.proba), 2);
                                }

                            }
                            if (prevState.proba < 0.001f)
                                newListState.RemoveAt(prevIndex);
                            else
                            {
                                newListState[prevIndex] = prevState;
                                maxProba += prevState.proba;
                            }

                        }
                        else
                        {
                            newListState.Add(newState);
                            maxProba += newState.proba;
                        }


                    }

                }
            }
            bool mode1IsFull = false;
            bool mode2IsFull = false;

            for (int i = 0; i < newListState.Count; i++)
            {
                if (!mode1IsFull && newListState[i].numberOfQubits[indexOfMode1] != 0)
                    mode1IsFull = true;
                if (!mode2IsFull && newListState[i].numberOfQubits[indexOfMode2] != 0)
                    mode2IsFull = true;
                newListState[i].proba = newListState[i].proba / maxProba;
            }
            owner1.stateList = newListState;
            if (!mode1IsFull)
            {
                Faketon.TraceOverMode(modeReflectedPositively);
                modeReflectedNegatively.TriggerCycle();
                modeReflectedNegatively.Subscribe();
            }
            else if (!mode2IsFull)
            {
                Faketon.TraceOverMode(modeReflectedNegatively);
                modeReflectedPositively.TriggerCycle();
                modeReflectedPositively.Subscribe();
            }
            else
            {
                modeReflectedNegatively.TriggerCycle();
                modeReflectedNegatively.Subscribe();
                modeReflectedPositively.TriggerCycle();
                modeReflectedPositively.Subscribe();
            }
            //owner1.DrawFaketon();
            Faketon.SeparateFaketon(owner1);
            //owner1.Print();
        }
        else
        {
            Faketon newFaketon = Faketon.Fuse(owner1, owner2);
            newFaketon.transform.parent = _levelManager.levelGrid.transform;
            //newFaketon.transform.position = transform.position;
            TwoMode(modeReflectedPositively, modeReflectedNegatively);
        }
    }

    public override void SetSpecificData(int[] specificData)
    {
        //throw new System.NotImplementedException();
    }


    private float Newton(int k, int n)
    {
        int result;
        result = Fact(n, k) / Fact(n - k, 1);
        return ((float)result);
    }

    private int Fact(int n, int k)
    {
        if (n <= k)
            return (1);
        else
            return (n * Fact(n - 1, k));
    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        collision.gameObject.TryGetComponent<SpatialMode>(out var newlyEnteredSpatialMode);
        if (newlyEnteredSpatialMode && !_createdModes.Contains(newlyEnteredSpatialMode))
        {
            newlyEnteredSpatialMode.AddInstrument(this);
            newlyEnteredSpatialMode.Unsubscribe();
            if (!_isTriggered)
            {
                _isTriggered = true;
                SubscribeCycle();
            }

            _inputModes[(int)newlyEnteredSpatialMode.orientation] = newlyEnteredSpatialMode;
        }
        else if (newlyEnteredSpatialMode)
        {
            _createdModes.Remove(newlyEnteredSpatialMode);
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        SpatialMode mode = collision.GetComponent<SpatialMode>();
        mode.RemoveInstrument(this);
    }

    public void SubscribeCycle()
    {
        _levelManager.StartCycle += TriggerCycle;
    }

    public void UnsubscribeCycle()
    {
        _levelManager.StartCycle -= TriggerCycle;
    }

    public void SubscribeClear()
    {
        _levelManager.ClearEvent += TriggerClear;
    }

    public void UnsubscribeClear()
    {
        _levelManager.ClearEvent -= TriggerClear;
    }

    public void TriggerClear()
    {
        UnsubscribeCycle();
        for (int i = 0; i < 4; i++)
            _inputModes[i] = null;
        _isTriggered = false;
    }

}
