﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class SwitchInOne : Instrument, ITriggerClear, ITriggerCycle, IClassicalReceptor
{
    public int numberOfCycleToChange = 3;

    private Connector _connector;
    private bool _alreadySubscribe;
    private int _prevCycle;
    //private int _elapsedCycle;
    private SpatialMode _inputMode;
    private bool _hasToRotateOnClear = false;

    [SerializeField] Slider _frequencySlider;

    public override void InitInstrument()
    {
        _connector = GetComponentInChildren<Connector>();
        _specificInstruData = new int[1];
        /*if (_frequencySlider == null)
            Debug.LogError("Error: Slider unassigned to object " + name);
        else
        {
            _frequencySlider.InitSlider(this);
        }*/
        SubscribeClear();
        //SubscribeCycle();
    }

    public void TriggerCycle()
    {
        if (_inputMode != null && _inputMode.isDestroyed == true)
        {
            _inputMode = null;
            return;
        }


        if (_inputMode != null)
        {
            _inputMode.orientation = ReflectOrientation(_inputMode.orientation);

            //if (numberOfCycleToChange <= 0)
             UnsubscribeCycle();

            _inputMode.TriggerCycle();
            _inputMode.Subscribe();
            _inputMode = null;
        }
    }

    public void TriggerClear()
    {
        UnsubscribeCycle();
        _inputMode = null;
        if (_hasToRotateOnClear)
            RotateClockwise(true);
        _hasToRotateOnClear = false;
    }


    public Orientation ReflectOrientation(Orientation inputOrientation)
    {
        if (m_orientation == Orientation.Left || m_orientation == Orientation.Right)
        {
            return inputOrientation switch
            {
                Orientation.Left => (Orientation.Down),
                Orientation.Right => (Orientation.Up),
                Orientation.Down => (Orientation.Left),
                Orientation.Up => (Orientation.Right),
                _ => (Orientation.Right),
            };
        }
        else
        {
            return inputOrientation switch
            {
                Orientation.Left => (Orientation.Up),
                Orientation.Right => (Orientation.Down),
                Orientation.Down => (Orientation.Right),
                Orientation.Up => (Orientation.Left),
                _ => (Orientation.Right),
            };
        }
    }

    public void SetIsControlled(bool isControlled)
    {
       /* if (isControlled)
        {
            _prevCycle = numberOfCycleToChange;
            _frequencySlider.ChangeValue(-1);
            _frequencySlider.IsInteractable = false;
            UnsubscribeCycle();
        }
        else
        {
            _frequencySlider.IsInteractable = true;
            if (_prevCycle > 0)
                _frequencySlider.ChangeValue(_prevCycle);
            else
                _frequencySlider.ChangeValue(3);
            SubscribeCycle();
        }*/
    }

    public override void SetSpecificData(int[] specificData)
    {
       /* if (specificData == null)
            Debug.LogError("Wesh, les datas ne sont pas init, y'a un prob");
        _frequencySlider.ChangeValue(specificData[0]);*/
    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        _inputMode = collision.gameObject.GetComponent<SpatialMode>();

        //if (numberOfCycleToChange <= 0)
            SubscribeCycle();
        _inputMode.Unsubscribe();
    }

    public void ChangeFrequency(int newValue)
    {
        _specificInstruData[0] = newValue;
        if (numberOfCycleToChange > 0)
            SubscribeCycle();
        else
            UnsubscribeCycle();
        numberOfCycleToChange = newValue;
    }


    public void ReceiveSignal(bool signal)
    {
        if (signal == true)
        {
            _hasToRotateOnClear ^= true;
            RotateClockwise(true);
        }

    }

    public void SubscribeCycle()
    {
        if (!_alreadySubscribe)
        {
            _alreadySubscribe = true;
            _levelManager.StartCycle += TriggerCycle;
        }

    }

    public void UnsubscribeCycle()
    {
        if (_alreadySubscribe)
        {
            _alreadySubscribe = false;
            _levelManager.StartCycle -= TriggerCycle;
        }
    }

    public void SubscribeClear()
    {
        _levelManager.ClearEvent += TriggerClear;
    }

    public void UnsubscribeClear()
    {
        _levelManager.ClearEvent -= TriggerClear;
    }

}
