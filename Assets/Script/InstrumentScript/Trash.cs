using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trash : Instrument, ITriggerCycle
{
    private List<SpatialMode> _inputModes;
    private bool _isTriggered = false;

    public override void InitInstrument()
    {
        _inputModes = new List<SpatialMode>();
    }

    public void TriggerCycle()
    {
        int i = 0;
        while (i < _inputModes.Count)
        {
            if (_inputModes[i].isDestroyed == true)
                _inputModes.RemoveAt(i);
            else
                i = i + 1;
        }
        foreach (SpatialMode mode in _inputModes)
        {

            if (!mode.isDestroyed)
                OneMode(mode);
        }
        _inputModes.Clear();
        _isTriggered = false;
        UnsubscribeCycle();
    }

    //TEMP
    //For now we consider hard limit of number of photon being 10

    public void OneMode(SpatialMode mode)
    {
        SpatialMode.TrashMode(mode);
    }


    public override void SetSpecificData(int[] specificData)
    {
        //throw new System.NotImplementedException();
    }
    public override void OnTriggerEnter2D(Collider2D collision)
    {
        var newlyEnteredSpatialMode = collision.gameObject.GetComponent<SpatialMode>();
        newlyEnteredSpatialMode.AddInstrument(this);
        if (!_isTriggered)
        {
            _isTriggered = true;
            SubscribeCycle();
        }
        newlyEnteredSpatialMode.Unsubscribe();
        _inputModes.Add(newlyEnteredSpatialMode);

    }

    public void SubscribeCycle()
    {
        _levelManager.StartCycle += TriggerCycle;
    }

    public void UnsubscribeCycle()
    {
        _levelManager.StartCycle -= TriggerCycle;
    }
}
