﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class Spawner : Instrument, ITriggerClear, ITriggerCycle, IClassicalReceptor
{
    private SpatialMode[] _inputModes;
    private int _elapsedCycle;
    private Connector _connector;
    private bool _isSubscribed = false;
    private bool _isControlled = false;
    private int _prevCycle = -1;
    private bool _alreadyEmitted = false;

    [SerializeField] Slider _frequencySlider;
    [SerializeField] Slider _launchOnStartSlider;
    [SerializeField] Sprite[] _cadrantsSprite;
    [SerializeField] SpriteRenderer _rendererCadrant;

    public int cycleToWait = -1;


    public override void InitInstrument()
    {
        _connector = GetComponentInChildren<Connector>();
        _elapsedCycle = 0;
        _specificInstruData = new int[2];
        if (_frequencySlider == null)
            Debug.LogError("Error: Slider unassigned to object " + name);
        else
        {
            _frequencySlider.InitSlider(this);
        }
        if (_launchOnStartSlider == null)
            Debug.LogError("Error: Slider unassigned to object " + name);
        else
        {
            _launchOnStartSlider.InitSlider(this);
        }
        SubscribeCycle();
        SubscribeClear();
    }

    public void TriggerCycle()
    {
        Faketon createdFaketon;

        _elapsedCycle += 1;
        if (_alreadyEmitted)
        {
            if (_elapsedCycle < 3)
                return;
            _alreadyEmitted = false;
            _rendererCadrant.sprite = _cadrantsSprite[8];
            UnsubscribeCycle();
            return;
        }
        
        if (_elapsedCycle == cycleToWait || _isControlled)
        {
            _elapsedCycle = 0;
            createdFaketon = GameObject.Instantiate(associatedFaketon.gameObject, Vector3.zero, Quaternion.identity, _levelManager.levelGrid.transform).GetComponent<Faketon>();
            int i = 0;
            foreach (SpatialMode mode in createdFaketon.spatialModes)
            {
                ConfigNewMode(mode, false, i);
                i = i + 1;
            }
            createdFaketon.DrawFaketon();
            if (_isControlled)
            {
                _elapsedCycle = 0;
                _rendererCadrant.sprite = _cadrantsSprite[7];
                _alreadyEmitted = true;
                return;
            }

        }
        _rendererCadrant.sprite = _cadrantsSprite[cycleToWait - _elapsedCycle];

    }

    private void ConfigNewMode(SpatialMode mode, bool isClassicalyCreated = false, int numberOfRota = 0)
    {
        mode.Unsubscribe();
        mode.AddInstrument(this);
        mode.SwitchToCollidingLayer();
        mode.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 1f);
        mode.ResetNextLocation();
        mode.orientation = m_orientation;
        for (int j = 0; j < numberOfRota; j++)
        {
            mode.RotateClockwise();
        }
		if (!isClassicalyCreated)
        	mode.TriggerCycle();
        mode.Subscribe();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {

        collision.GetComponent<SpatialMode>().RemoveInstrument(this);
    }

    public override void SetSpecificData(int[] specificData)
    {
        _frequencySlider.ChangeValue(specificData[0]);
        _launchOnStartSlider.ChangeValueBool(specificData[1]);
    }

    public void ReceiveSignal(bool signal)
    {
        Faketon createdFaketon;

        if (signal && !_alreadyEmitted)
        {
            _elapsedCycle = 0;
            _rendererCadrant.sprite = _cadrantsSprite[7];
            _alreadyEmitted = true;
            SubscribeCycle();
            createdFaketon = GameObject.Instantiate(associatedFaketon.gameObject, transform.position, Quaternion.identity, _levelManager.levelGrid.transform).GetComponent<Faketon>();
           	int i = 0;
            foreach (SpatialMode mode in createdFaketon.spatialModes)
            {
                ConfigNewMode(mode, true, i);
                i = i + 1;
            }
            createdFaketon.DrawFaketon();
        }
    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {
    }

    public void SetIsControlled(bool isControlled)
    {
        if (isControlled)
        {
            //_frequencySlider.gameObject.transform.parent.gameObject.SetActive(false);
            //_launchOnStartSlider.gameObject.transform.parent.gameObject.SetActive(true);
            _prevCycle = cycleToWait;
            _frequencySlider.ChangeValue(-1);
            _frequencySlider.IsInteractable = false;
        }
        else
        {
            //_frequencySlider.gameObject.transform.parent.gameObject.SetActive(true);
            //_launchOnStartSlider.gameObject.transform.parent.gameObject.SetActive(false);
            _frequencySlider.IsInteractable = true;
            if (_prevCycle > 0)
                _frequencySlider.ChangeValue(_prevCycle);
            else
                _frequencySlider.ChangeValue(3);
        }
        _isControlled = isControlled;
    }

    public void ChangeFrequency(int newValue)
    {
        _specificInstruData[0] = newValue;
        cycleToWait = newValue;
        if (newValue == -1)
            _rendererCadrant.sprite = _cadrantsSprite[8];
        else
            _rendererCadrant.sprite = _cadrantsSprite[newValue];
    }




    #region Drag Functions

    public override void BeginDrag(Vector3 position, Vector2 mousePosition)
    {
        _initialPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        spriteObject.sortingLayerName = "FloatingObjects";
        _rendererCadrant.sortingLayerName = "FloatingObjects";
        _levelManager.gridHandler.FreeCell(transform.position);
        transform.Translate(Vector3.forward * -5f);
        noDrag = true;
    }

    public override void EndDrag(Vector3 position, Vector2 mousePosition)
    {
        Vector2Int gridPosition = _levelManager.gridHandler.WorldToGridCoord(position);
        Ray inputRay = Camera.main.ScreenPointToRay(mousePosition);
        RaycastHit2D hitResults = new RaycastHit2D();

        spriteObject.sortingLayerName = "Instruments";
        _rendererCadrant.sortingLayerName = "Instruments";
        transform.Translate(Vector3.forward * 5f);
        hitResults = Physics2D.Raycast(inputRay.origin, inputRay.direction, 1000f, LayerMask.GetMask("GameUI"));
        if (!isDraggable || _levelManager.gridHandler.IsLocked(gridPosition) || hitResults.transform != null)
        {
            if (isOnGrid == false || hitResults.transform != null)
                Instrument.Destroy(this);
            else
            {
                transform.position = (Vector3)_initialPosition;
                _levelManager.gridHandler.LockCell((Vector3)_initialPosition, this);
            }

        }
        else
        {
            transform.position = _levelManager.gridHandler.NearestGridCenter(gridPosition);
            Vector2Int initialGrid = _levelManager.gridHandler.WorldToGridCoord((Vector3)_initialPosition);
            if (isOnGrid && initialGrid != gridPosition)
                _historyManager.AddMoveInstrumentAction(gridPosition, initialGrid);
            else if (isOnGrid == false)
                _historyManager.AddSpawnInstrumentAction(gridPosition);
            _levelManager.gridHandler.LockCell(gridPosition, this);
        }
        if (noDrag)
        {
            if (!InputManager.IsSelected(gameObject))
            {
                InputManager.ClearSelection();
                selector.SetActive(true);
                InputManager.AddToSelection(gameObject);
            }
            else
                InputManager.ClearSelection();
        }
    }

    #endregion

    public override Orientation RotateClockwise(bool forceRotation = false)
    {
        if (isDraggable || forceRotation)
        {
            spriteObject.transform.Rotate(Vector3.forward * -90f);
            _rendererCadrant.transform.Rotate(Vector3.forward * 90f);

            m_orientation = m_orientation switch
            {
                Orientation.Left => (Orientation.Up),
                Orientation.Right => (Orientation.Down),
                Orientation.Down => (Orientation.Left),
                Orientation.Up => (Orientation.Right),
                _ => (Orientation.Right),
            };
        }
        return (m_orientation);
    }

    public override Orientation RotateCounterClockwise(bool forceRotation = false)
    {
        if (isDraggable || forceRotation)
        {
            spriteObject.transform.Rotate(Vector3.forward * 90f);
            _rendererCadrant.transform.Rotate(Vector3.forward * -90f);

            m_orientation = m_orientation switch
            {
                Orientation.Left => (Orientation.Down),
                Orientation.Right => (Orientation.Up),
                Orientation.Down => (Orientation.Right),
                Orientation.Up => (Orientation.Left),
                _ => (Orientation.Right),
            };
        }
        return (m_orientation);
    }


    public void SubscribeCycle()
    {
        if (!_isSubscribed)
        {
            _isSubscribed = true;
            _levelManager.StartCycle += TriggerCycle;
        }
    }

    public void UnsubscribeCycle()
    {
        if (_isSubscribed)
        {
            _isSubscribed = false;
            _levelManager.StartCycle -= TriggerCycle;
        }

    }


    public void SubscribeClear()
    {
        _levelManager.ClearEvent += TriggerClear;
    }

    public void UnsubscribeClear()
    {
        _levelManager.ClearEvent -= TriggerClear;
    }

    public void TriggerClear()
    {
        _alreadyEmitted = false;
        SubscribeCycle();
        if (_isControlled)
        {
            _rendererCadrant.sprite = _cadrantsSprite[8];
        }
        else
        {
            _elapsedCycle = cycleToWait - 1;
            _rendererCadrant.sprite = _cadrantsSprite[_specificInstruData[0]];
        }
            
    }
}
