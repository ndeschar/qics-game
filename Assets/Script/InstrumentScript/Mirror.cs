﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class Mirror : Instrument
{

    public override void InitInstrument()
    {

    }


    public Orientation ReflectOrientation(Orientation inputOrientation)
    {
        if (m_orientation == Orientation.Left || m_orientation == Orientation.Right)
        {
            return inputOrientation switch
            {
                Orientation.Left => (Orientation.Down),
                Orientation.Right => (Orientation.Up),
                Orientation.Down => (Orientation.Left),
                Orientation.Up => (Orientation.Right),
                _ => (Orientation.Right),
            };
        }
        else
        {
            return inputOrientation switch
            {
                Orientation.Left => (Orientation.Up),
                Orientation.Right => (Orientation.Down),
                Orientation.Down => (Orientation.Right),
                Orientation.Up => (Orientation.Left),
                _ => (Orientation.Right),
            };
        }
    }

    public override void SetSpecificData(int[] specificData)
    {
        //throw new System.NotImplementedException();
    }
    public override void OnTriggerEnter2D(Collider2D collision)
    {
        var newlyEnteredSpatialMode = collision.gameObject.GetComponent<SpatialMode>();
        newlyEnteredSpatialMode.orientation = ReflectOrientation(newlyEnteredSpatialMode.orientation);
    }


}
