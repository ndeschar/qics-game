using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is meant to be used in the tutorial. The behaviour are hard coded.
/// </summary>
public class ClassicalBeamSplitter : Instrument, ITriggerCycle, ITriggerClear
{
    //private List<string> _createdModesName;
    private SpatialMode[] _inputModes;
    private bool _isTriggered = false;
    private List<SpatialMode> _createdModes;
    //private SpatialMode _lastCreatedMode;

    public override void InitInstrument()
    {
        //_createdModesName = new List<string>();
        _inputModes = new SpatialMode[4];
        _createdModes = new List<SpatialMode>();

        //_lastCreatedMode = null;
        for (int i = 0; i < 4; i++)
            _inputModes[i] = null;
        SubscribeCycle();
        SubscribeClear();
    }

    public void TriggerCycle()
    {
        UnsubscribeCycle();
        for (int i = 0; i < 4; i++)
        {
            if (_inputModes[i] && _inputModes[i].isDestroyed == true)
                _inputModes[i] = null;
        }

        Orientation currentUp = m_orientation.NextClockwiseOrientation();
        Orientation currentRight = currentUp.NextClockwiseOrientation();
        Orientation currentDown = currentRight.NextClockwiseOrientation();

        if (_inputModes[(int)currentUp])
            OneMode(_inputModes[(int)currentUp], -1, currentRight);
        else if (_inputModes[(int)currentRight])
            OneMode(_inputModes[(int)currentRight], 1, currentUp);
        else if (_inputModes[(int)m_orientation])
            OneMode(_inputModes[(int)m_orientation], -1, currentDown);
        else if (_inputModes[(int)currentDown])
            OneMode(_inputModes[(int)currentDown], 1, m_orientation);
        for (int i = 0; i < 4; i++)
            _inputModes[i] = null;
        _isTriggered = false;

    }

    public int modeId = 0;

    // 
    public void OneMode(SpatialMode mode, int phase, Orientation createdModeOrientation)
    {
        Faketon owner = mode.owner;
        List<State> newListState = new List<State>();

        float dice = Random.Range(0f, 1f);
        if (dice < 0.5f)
        {
            mode.orientation = createdModeOrientation;
            foreach (State state in owner.stateList)
            {
                State newState = new State(state);
                newState.phase *= phase;
                newListState.Add(newState);
            }
            owner.stateList = newListState;
        }
        owner.DrawFaketon();
        mode.TriggerCycle();
        mode.Subscribe();
    }

    public override void SetSpecificData(int[] specificData)
    {
        //throw new System.NotImplementedException();
    }


    private float Newton(int k, int n)
    {
        int result;
        result = Fact(n, k) / Fact(n - k, 1);
        return ((float)result);
    }

    private int Fact(int n, int k)
    {
        if (n <= k)
            return (1);
        else
            return (n * Fact(n - 1, k));
    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        collision.gameObject.TryGetComponent<SpatialMode>(out var newlyEnteredSpatialMode);
        if (newlyEnteredSpatialMode && !_createdModes.Contains(newlyEnteredSpatialMode))
        {
            newlyEnteredSpatialMode.AddInstrument(this);
            newlyEnteredSpatialMode.Unsubscribe();
            if (!_isTriggered)
            {
                _isTriggered = true;
                SubscribeCycle();
            }

            _inputModes[(int)newlyEnteredSpatialMode.orientation] = newlyEnteredSpatialMode;
        }
        else if (newlyEnteredSpatialMode)
        {
            _createdModes.Remove(newlyEnteredSpatialMode);
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        SpatialMode mode = collision.GetComponent<SpatialMode>();
        mode.RemoveInstrument(this);
    }

    public void SubscribeCycle()
    {
        _levelManager.StartCycle += TriggerCycle;
    }

    public void UnsubscribeCycle()
    {
        _levelManager.StartCycle -= TriggerCycle;
    }

    public void SubscribeClear()
    {
        _levelManager.ClearEvent += TriggerClear;
    }

    public void UnsubscribeClear()
    {
        _levelManager.ClearEvent -= TriggerClear;
    }

    public void TriggerClear()
    {
        UnsubscribeCycle();
        for (int i = 0; i < 4; i++)
            _inputModes[i] = null;
        _isTriggered = false;
    }

}
