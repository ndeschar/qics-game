﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class PhaserAlter : Instrument, ITriggerClear, ITriggerCycle, IClassicalReceptor
{
    private int _elapsedCycle = 0;
    private Connector _connector;
    private SpatialMode newlyEnteredSpatialMode;
    private bool _isTriggered = false;
    private List<SpatialMode> _inputModes;
    private bool _isControlled = false;
    private int _signalCountdown = 0;
    private int _originalPhase;
    private bool _alreadySubscribe = false;
    [SerializeField] Slider _frequencySlider;
    [SerializeField] Slider _initialStateSlider;

    public SpriteRenderer spriteRenderer;
    public Sprite onSprite;
    public Sprite offSprite;
    public int currentPhase = -1;
    public int numberOfCycleToChange = -1;


    public override void InitInstrument()
    {
        _connector = GetComponentInChildren<Connector>();
        _inputModes = new List<SpatialMode>();
        _specificInstruData = new int[2];
        if (_frequencySlider == null || _initialStateSlider == null)
            Debug.LogError("Error: Slider unassigned to object " + name);
        else
        {
            _frequencySlider.InitSlider(this);
            _initialStateSlider.InitSlider(this);
        }
        SubscribeClear();
        SubscribeCycle();
    }

    public void TriggerCycle()
    {
        int i = 0;
        bool goBackToOrigin;

        goBackToOrigin = false;

        while (i < _inputModes.Count)
        {
            if (_inputModes[i].isDestroyed == true)
                _inputModes.RemoveAt(i);
            else
                i = i + 1;
        }
        if (numberOfCycleToChange > 0)
        {
            if (!_isControlled)
                _elapsedCycle += 1;
            else if (_signalCountdown > 0)
            {
                _signalCountdown -= 1;
                if (_signalCountdown == 0)
                    goBackToOrigin = true;
            }

            if (_elapsedCycle == numberOfCycleToChange)
            {
                _elapsedCycle = 0;

                if (currentPhase == -1)
                    SwitchPhaserPhase(0);
                else
                    SwitchPhaserPhase(1);
            }
            else if (goBackToOrigin)
            {
                SwitchPhaserPhase(_specificInstruData[1]);
            }

            if (_isTriggered && currentPhase == -1)
            {

                foreach (SpatialMode mode in _inputModes)
                {
                    Faketon owner;
                    int indexMode;

                    owner = mode.owner;
                    indexMode = owner.spatialModes.IndexOf(mode);
                    foreach (State state in owner.stateList)
                    {
                        if (state.numberOfQubits[indexMode] % 2 == 1)
                            state.SwitchPhase(currentPhase);
                    }
                    if (mode != null)
                    {
                        mode.owner.DrawFaketon();
                        mode.TriggerCycle();
                        mode.Subscribe();
                    }
                }
            }
            else
            {
                foreach (SpatialMode mode in _inputModes)
                {
                    if (mode != null)
                    {
                        mode.owner.DrawFaketon();
                        mode.TriggerCycle();
                        mode.Subscribe();
                    }
                }
            }


        }
        else
        {
            if (_isTriggered && currentPhase == -1)
            {
                foreach (SpatialMode mode in _inputModes)
                {
                    Faketon owner;
                    int indexMode;

                    owner = mode.owner;
                    indexMode = owner.spatialModes.IndexOf(mode);
                    foreach (State state in owner.stateList)
                    {
                        if (state.numberOfQubits[indexMode] % 2 == 1)
                            state.SwitchPhase(currentPhase);
                    }
                    if (mode != null)
                    {
                        mode.owner.DrawFaketon();
                        mode.TriggerCycle();
                        mode.Subscribe();
                    }
                }
                UnsubscribeCycle();
            }
            else
            {
                foreach (SpatialMode mode in _inputModes)
                {
                    if (mode != null)
                    {
                        mode.owner.DrawFaketon();
                        mode.TriggerCycle();
                        mode.Subscribe();
                    }
                }
            }

        }
        _isTriggered = false;
        _inputModes.Clear();

    }



    public void SetIsControlled(bool isControlled)
    {
        if (isControlled)
        {
            _isControlled = true;
            //UnsubscribeCycle();
        }
        else
        {
            _isControlled = false;
            //SubscribeCycle();
        }
    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {

        newlyEnteredSpatialMode = collision.gameObject.GetComponent<SpatialMode>();

        if (!_isTriggered)
        {
            _isTriggered = true;
            if (numberOfCycleToChange <= 0)
                SubscribeCycle();
        }
        _inputModes.Add(newlyEnteredSpatialMode);
        newlyEnteredSpatialMode.Unsubscribe();

    }

    public void ReceiveSignal(bool signal)
    {
        if (signal == true)
        {
            SwitchPhaserPhase(_specificInstruData[1] ^ 1);
        }
       /* else
            SwitchPhaserPhase(0);*/

        _signalCountdown = numberOfCycleToChange;
    }

    public void SwitchPhaserPhase(int newPhase)
    {
        if (newPhase == 0)
        {
            spriteRenderer.sprite = offSprite;
            currentPhase = 1;
        }
        else
        {
            spriteRenderer.sprite = onSprite;
            currentPhase = -1;
        }
    }

    public void ChangeFrequency(int newValue)
    {
        _specificInstruData[0] = newValue;
        numberOfCycleToChange = newValue;
    }

    public void ChangeInitialState(int newState)
    {
        _specificInstruData[1] = newState;
        SwitchPhaserPhase(newState);
    }

    public void SubscribeCycle()
    {
        if (!_alreadySubscribe)
        {
            _alreadySubscribe = true;
            _levelManager.StartCycle += TriggerCycle;
        }

    }

    public void UnsubscribeCycle()
    {
        _alreadySubscribe = false;
        _levelManager.StartCycle -= TriggerCycle;
    }

    public void SubscribeClear()
    {
        _levelManager.ClearEvent += TriggerClear;
    }

    public void UnsubscribeClear()
    {
        _levelManager.ClearEvent -= TriggerClear;
    }

    public override void SetSpecificData(int[] specificData)
    {
        if (specificData == null)
            Debug.LogError("Wesh, les datas ne sont pas init, y'a un prob");
        _frequencySlider.ChangeValue(specificData[0]);
        _initialStateSlider.ChangeValueBool(specificData[1]);
    }

    public void TriggerClear()
    {
        _elapsedCycle = 0;
        if (numberOfCycleToChange <= 0)
            UnsubscribeCycle();
        _isTriggered = false;
        _inputModes.Clear();
        SwitchPhaserPhase(_specificInstruData[1]);
    }
}
