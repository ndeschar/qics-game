using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface INeedGridHandler
{
    public void SetGridHandler(GridHandler handler);
}
