using System.Collections.Generic;
using UnityEngine;
using System.Linq;

//TODO: A different Grid Handler which pool from a set of objects
public class GridHandler : MonoBehaviour
{
    public List<GridData> GridDatas { get; set; }
    private InstrumentTicket[] _ticketsInLevel;
    /// <summary>
    /// Additional horizontal Square ON EACH SIDE
    /// </summary>
    [SerializeField, Range(0, 12)] int _additionalHorizontalSquare;
    /// <summary>
    /// Additional vertical Square ON EACH SIDE
    /// </summary>
    [SerializeField, Range(0, 10)] int _additionalVerticalSquare;
    [SerializeField] BoxCollider2D _upperLimit;

    private GridLimit _colliderGrid;
    private Vector2 _additionalPosition;
    private Vector2 _sizeOfMap = new Vector2(6, 5);
    private Vector3 _anchorUpperLimit;
    private Grid _levelGrid;

    public Grid LevelGrid
    {
        get 
        {
            if (_levelGrid == null)
                _levelGrid = GetComponent<Grid>();
            return (_levelGrid);
        }
    }

    private void Awake()
    {
        //Init();
    }

    private void Start()
    {
        _colliderGrid = GetComponentInChildren<GridLimit>();
        if (_colliderGrid == null)
            Debug.LogWarning("No Box collider found");
        else
            _colliderGrid.ResizeCollider(_sizeOfMap, new Vector2(_additionalHorizontalSquare, _additionalVerticalSquare), LevelGrid.cellSize.x);

    }

    public void Init()
    {
        _levelGrid = GetComponent<Grid>();
        GridDatas = new List<GridData>();
        // When the object is first created (even before Start()), put the object in the "Grid" layer
        gameObject.layer = LayerMask.NameToLayer("Grid");
        // Better to ensure that z=0, to ensure that the convertion Vector2/Vector3 is harmless.
        // (I don't think it's super important anyway)
        gameObject.transform.localPosition = new Vector3(.0f, .0f, 0f);
        _anchorUpperLimit = _upperLimit.transform.position;
        _additionalPosition = new Vector2(_additionalHorizontalSquare * LevelGrid.cellSize.x, _additionalVerticalSquare * LevelGrid.cellSize.y);
        // We use the great "Sorting Layer" component to display automatically the objects
        // in the right order (requires 'using UnityEngine.Rendering;')

    }

    #region Grid Utility

    // This function turns a (world) position into an interpolated coordinate in the grid
    // (so is a float, we don't round anything)
    // We use the convention that all coordinates are specified world-wise (i.e. via transform.position)
    public Vector2Int WorldToGridCoord(Vector3 position)
    {
        // We use the library function, and we cast to vector 2 (get rids of the y coordinate)
        return ((Vector2Int)LevelGrid.WorldToCell(position));
    }

    public Vector3 GridCoordToWorld(Vector2Int gridPosition)
    {
        return (LevelGrid.GetCellCenterWorld((Vector3Int)gridPosition));
    }

    public Vector3 GridCoordToWorldNotCentered(Vector2Int gridPosition)
    {
        return (LevelGrid.CellToWorld((Vector3Int)gridPosition));
    }

    // I use the convention that coordinates are specified world-wise (i.e. with transform.position)
    public Vector3 NearestGridCenter(Vector3 position)
    {
        // First we convert to cell (integer: we round), then we convert back into the world coordinates
        return (GridCoordToWorld(WorldToGridCoord(position)));
    }

    // I use the convention that coordinates are specified world-wise (i.e. with transform.position)
    public Vector3 NearestGridCenter(Vector2Int gridPosition)
    {
        // We use here the fact we can cast Vector2 to Vector3 and vice versa (just add 0 on z/remove z)
        return (GridCoordToWorld(gridPosition));
    }

    public bool IsLocked(Vector2Int coordToCheck)
    {
        return (GridDatas.Exists((data) => data.Coord == coordToCheck));
    }

    public int IndexOfCoord(Vector2Int coordToCheck)
    {
        return (GridDatas.FindIndex((data) => data.Coord == coordToCheck));
    }

    public Instrument GetInstrumentFromIndex(int index)
    {
        return (GridDatas[index].AssociatedInstrument);
    }

    public GridData GetGridDataFromCoord(Vector2Int coordToCheck)
    {
        return (GridDatas[IndexOfCoord(coordToCheck)]);
    }

    public int IndexOfInstrument(Instrument instrument)
    {
        return (GridDatas.FindIndex((data) => data.AssociatedInstrument == instrument));
    }

    public Instrument GetInstrumentFromCoord(Vector2Int coordToCheck)
    {
        return (GetInstrumentFromIndex(IndexOfCoord(coordToCheck)));
    }

    public bool LockCell(Vector2Int coordToLock, Instrument instrument)
    {
        if (IsLocked(coordToLock))
            return (false);
        else
        {
            GridDatas.Add(new GridData(coordToLock, instrument.associatedTicket.GetId(), instrument));
           
            instrument.isOnGrid = true;
            return (true);
        }
    }

    public bool LockCell(Vector3 posToLock, Instrument instrument)
    {
        Vector2Int coordToLock = WorldToGridCoord(posToLock);

        return (LockCell(coordToLock, instrument));
    }

    public bool FreeCell(Vector2Int coordToFree)
    {
        int index = IndexOfCoord(coordToFree);

        if (index >= 0)
        {
            GridDatas.RemoveAt(index);
            //_lockedCoord.Remove(coordToFree);
            return (true);
        }
        else
        {
            return (false);
        }
    }

    public bool FreeCell(Vector3 posToFree)
    {
        Vector2Int coordToFree = WorldToGridCoord(posToFree);

        return (FreeCell(coordToFree));

    }

    public float GetGridSize()
    {
        return (LevelGrid.cellSize.x);
    }

    public void PrintGrid()
    {
        foreach (GridData data in GridDatas)
        {
            Debug.Log("At " + data.Coord + " there is " + data.AssociatedInstrument + " instrument of id " + data.instrumentId + " with orientation " + data.instrumentOrientation);
        }
    }

    public static void PrintGrid(List<GridData> gridDatas)
    {
        foreach (GridData data in gridDatas)
        {
            Debug.Log("At " + data.Coord + " instrument of id " + data.instrumentId + " with orientation " + data.instrumentOrientation);
        }
    }

    public void ChangeDataOrientation(Vector3 position, Orientation newOrientation)
    {
        Vector2Int coord = WorldToGridCoord(position);
        GridData dataToChange = GridDatas.Find((data) => data.Coord == coord);
        dataToChange.instrumentOrientation = newOrientation;
    }
    #endregion

    public void MoveGrid(Vector3 newPosition)
    {
        Vector3 clampedPosition = new Vector3(Mathf.Clamp(newPosition.x, -_additionalPosition.x, _additionalPosition.x), Mathf.Clamp(newPosition.y, -_additionalPosition.y, _additionalPosition.y), newPosition.z);
        transform.position = clampedPosition;
        _upperLimit.transform.position = Vector3.up * (Camera.main.orthographicSize + 0.5f);
    }

    public void UpdateZoom(float newZoom)
    {
        _additionalPosition = new Vector2(_sizeOfMap.x * LevelGrid.cellSize.x * (1 - newZoom) + _additionalHorizontalSquare * LevelGrid.cellSize.x, _additionalVerticalSquare * LevelGrid.cellSize.y + _sizeOfMap.y * LevelGrid.cellSize.y * (1 - newZoom));
        MoveGrid(transform.position);
    }

    public void SetInstrumentsOnGrid(List<GridData> gridDatas)
    {
        foreach (GridData gridData in gridDatas)
        {
            InstrumentTicket instrumentTicket = _ticketsInLevel.First((ticket) => (ticket.GetId() == gridData.instrumentId));
            if (instrumentTicket == null)
                Debug.Log("No ticket with id " + gridData.instrumentId);
            else
            {
                Transform newInstrumentTransform;

                if (instrumentTicket is TicketReceptorMulti)
                    newInstrumentTransform = ((TicketReceptorMulti) instrumentTicket).InstantiateDraggableObject(GridCoordToWorld(gridData.Coord));
                else
                    newInstrumentTransform = instrumentTicket.InstantiateDraggableObject(GridCoordToWorld(gridData.Coord));
                if (newInstrumentTransform == null)
                {
                    Debug.Log("Could not instantiate instrument associated to ticket " + instrumentTicket.name);
                }
                else
                {
                    Instrument newInstrument = newInstrumentTransform.GetComponent<Instrument>();
                    newInstrument.isDraggable = gridData.isDraggable;
                    newInstrument.GiveOrientation(gridData.instrumentOrientation, true);
                    newInstrument.SetSpecificData(gridData.specificInstruData);
                    LockCell(gridData.Coord, newInstrument);
                }

            }
        }
    }

    public List<GridData> GetNonDevPlacedInstrument()
    {
        List<GridData> cleanedDatas = new List<GridData>();

        foreach (GridData data in GridDatas)
        {
            if (data.AssociatedInstrument.isDraggable)
            {
                cleanedDatas.Add(data);
            }
                
        }
        return (cleanedDatas);
    }

    public InstrumentTicket GetInstrumentTicketFromId(int id)
    {
        return (_ticketsInLevel.First((ticket) => (ticket.GetId() == id)));
    }

    public Instrument SetInstrumentOnGrid(GridData gridData)
    {
        InstrumentTicket instrumentTicket = _ticketsInLevel.First((ticket) => (ticket.GetId() == gridData.instrumentId));
        if (instrumentTicket == null)
            Debug.Log("No ticket with id " + gridData.instrumentId);
        else
        {
            Transform newInstrumentTransform = instrumentTicket.InstantiateDraggableObject(GridCoordToWorld(gridData.Coord));
            if (newInstrumentTransform == null)
            {
                Debug.Log("Could not instantiate instrument associated to ticket " + instrumentTicket.name);
            }
            else
            {
                Instrument newInstrument = newInstrumentTransform.GetComponent<Instrument>();
                newInstrument.isDraggable = gridData.isDraggable;
                newInstrument.GiveOrientation(gridData.instrumentOrientation, true);
                newInstrument.SetSpecificData(gridData.specificInstruData);
                LockCell(gridData.Coord, newInstrument);
                return (newInstrument);
            }

        }
        return (null);
    }

    public void MoveInstrumentOnGrid(Vector2Int currentPosition, Vector2Int newPosition)
    {
        if (!IsLocked(currentPosition))
        {
            Debug.LogError("No instrument at coordinate " + currentPosition.ToString());
            return;
        }
        if (IsLocked(newPosition))
        {
            Debug.LogError("Unexpected instrument at coordinate " + currentPosition.ToString());
            return;
        }
        Instrument instrumentToMove = GetInstrumentFromCoord(currentPosition);
        FreeCell(currentPosition);
        instrumentToMove.transform.position = GridCoordToWorld(newPosition);
        LockCell(newPosition, instrumentToMove);
    }

    public void RemoveInstrumentOnGrid(Vector2Int position)
    {
        if (!IsLocked(position))
        {
            Debug.LogError("No instrument at coordinate " + position.ToString());
            return;
        }
        Instrument instrumentToDestroy = GetInstrumentFromCoord(position);
        FreeCell(position);
        Instrument.Destroy(instrumentToDestroy);
    }
    public void SetTickets(InstrumentTicket[] tickets)
    {
        _ticketsInLevel = tickets;
    }
}
