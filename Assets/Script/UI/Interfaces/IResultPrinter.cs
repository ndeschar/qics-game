using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IResultPrinter
{
    public void PrintResults(int[] results, float[] comparedPerf);
}
