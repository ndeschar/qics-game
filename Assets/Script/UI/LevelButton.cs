using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;



public class LevelButton : MonoBehaviour
{
    [SerializeField] string _associatedLevel = "Level1000";
    [SerializeField] Button button;

    public string AssociatedLevel { get { return (_associatedLevel); } }
    
    public void SetInteractable(bool is_interactable)
    {
        button.interactable = is_interactable;
    }
}
