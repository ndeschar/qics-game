using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class StateWriter : MonoBehaviour
{
    public Instrument associatedInstrument;
    public Faketon faketonToWrite;
    public TextMeshPro numberOfFaketonText;
    public TextMeshPro stateText;

    public void Awake()
    {
        if (faketonToWrite == null)
        {
            Debug.LogError("No associated faketon to " + name);
            Destroy(gameObject);
        }
        WriteState();
    }

    public void WriteState()
    {
        if (associatedInstrument != null)
        {
            faketonToWrite = associatedInstrument.associatedFaketon;
        }
        if (numberOfFaketonText != null)
        {
            if (faketonToWrite.numberOfSpatialModes == 1)
                numberOfFaketonText.text = "�met 1 Fauxton :";
            else
                numberOfFaketonText.text = "�met " + faketonToWrite.numberOfSpatialModes.ToString() + " Fauxtons :";
        }
        if (stateText != null)
        {
            stateText.text = faketonToWrite.ToString();
        }
    }
}
