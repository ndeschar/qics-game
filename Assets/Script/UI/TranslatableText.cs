using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TranslatableText : MonoBehaviour
{
    [SerializeField] List<TMP_Text> _textObjs;
    [TextArea(5, 20), SerializeField] string[] _texts;

    private GameManager _gameManager;

    public void Awake()
    {
        if (_textObjs == null)
            _textObjs = new List<TMP_Text>();
        if (_textObjs.Count == 0)
        {
            _textObjs.Add(GetComponent<TMP_Text>());
            Debug.LogWarning("No text object for " + transform.parent.parent.name + " had to add it at runtime");
        }

        for (int i = 0; i < _textObjs.Count; i++)
        {
            if (_textObjs[i] == null)
            {
                _textObjs[i] = GetComponent<TMP_Text>();
                Debug.LogWarning("No text object for " + transform.parent.parent.name + " had to add it at runtime");
            }
        }
        if (_texts.Length == 0)
            Debug.LogError("No enough text in TranslatableText " + transform.parent.parent.name);
    }

    public void OnEnable()
    {
        _gameManager = GameManager.Instance;
        _gameManager.TranslateEvent += Translate;
        Translate();
    }

    public void Translate()
    {
        int indexLanguage;

        indexLanguage = (int)_gameManager.CurrentLanguage;
        if (indexLanguage > _texts.Length)
        {
            Debug.LogWarning("Unimplemented language, default to French");
            indexLanguage = 0;
        }
        foreach (var textObj in _textObjs)
        {
            textObj.text = _texts[indexLanguage];
        }
    }
}
