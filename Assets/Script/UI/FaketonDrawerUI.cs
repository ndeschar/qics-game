using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FaketonDrawerUI : MonoBehaviour
{
    [SerializeField] Instrument _associatedInstrument;
    [SerializeField] Faketon _associatedFaketon;
    [SerializeField] float _radius = 0.8f;
    [SerializeField] InstrumentTicket _associatedTicket = null;
    private Faketon _faketonToDraw;
    private bool _isInInstrument = false;

    private void Awake()
    {
        if (_associatedTicket != null)
        {
            _associatedFaketon = _associatedTicket.associatedFaketon;
            _faketonToDraw = Instantiate(_associatedFaketon, Vector3.zero, Quaternion.identity, transform);
        }
        else if (_associatedInstrument == null)
        {
            _isInInstrument = true;
            if (_associatedFaketon != null)
                _faketonToDraw = Instantiate(_associatedFaketon, Vector3.zero, Quaternion.identity, transform);
            else
            {
                _associatedInstrument = GetComponentInParent<Instrument>();
                _faketonToDraw = Instantiate(_associatedFaketon, Vector3.zero, Quaternion.identity, transform);
            }
        }
        else
        {
            _isInInstrument = true;
            _associatedFaketon = _associatedInstrument.associatedTicket.associatedFaketon;
            _faketonToDraw = Instantiate(_associatedFaketon, Vector3.zero, Quaternion.identity, transform);
        }
        _faketonToDraw.transform.position = transform.position;
        float upperOffset;
        if (_isInInstrument)
            upperOffset = 0.7f;
        else
            upperOffset = 1.28f;
        int i = 0;
        foreach (SpatialMode mode in _faketonToDraw.spatialModes)
        {
            mode.SwitchToIgnoreLayer();
            mode.transform.position = transform.position;
            mode.transform.localPosition = new Vector3(1.28f * (i % 2), upperOffset * Mathf.Ceil(i / 2), 0f);
            i++;
        }

        //Temporary measure since I did not thought about having faketon I would not want to clear
        LevelManager.Instance.RemoveFaketon(_faketonToDraw);
        // C'est moche mais �a passe
        LineRenderer lineRender  = _faketonToDraw.gameObject.GetComponent<LineRenderer>();
        if (lineRender != null)
            lineRender.sortingLayerName = "GameUI";
        _faketonToDraw.DrawFaketon(_radius);
    }

    public void RedrawWithPhase()
    {
        List<State> newStateList = new List<State>();

        foreach (State state in _faketonToDraw.stateList)
        {
            State newState = new State(state);
            newState.phase *= -1;
            newStateList.Add(newState);
        }
        _faketonToDraw.stateList = newStateList;
        _faketonToDraw.DrawFaketon(_radius);
    }

    public void SetFaketonToDraw(Faketon faketon)
    {
        if (_faketonToDraw != null)
        {
            _faketonToDraw.stateList = faketon.stateList;
            _faketonToDraw.DrawFaketon();
        }
    }

    public void OnEnable()
    {
        _faketonToDraw.DrawLine();
    }
}
