using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(Faketon))]
public class StateDrawerUI : MonoBehaviour
{
    // ideally contain a simple faketon + mode + linerenderer
    private Faketon _faketonToDraw;
    private TMP_Text _probaText;
    private SpatialMode[] _allSpatialModes;
    private SpatialMode _selectedMode = null;

    /*[SerializeField] Instrument _associatedInstrument;
    [SerializeField] Faketon _associatedFaketon;
    [SerializeField] float _radius = 0.8f;
    private Faketon _faketonToDraw;*/

    /* private void Awake()
     {
         if (_associatedInstrument == null)
         {
             if (_associatedFaketon != null)
                 _faketonToDraw = Instantiate(_associatedFaketon, Vector3.zero, Quaternion.identity, transform);
             else
             {
                 _associatedInstrument = GetComponentInParent<Instrument>();
                 _faketonToDraw = Instantiate(_associatedFaketon, Vector3.zero, Quaternion.identity, transform);
             }
         }
         else
         {
             _associatedFaketon = _associatedInstrument.associatedTicket.associatedFaketon;
             _faketonToDraw = Instantiate(_associatedFaketon, Vector3.zero, Quaternion.identity, transform);
         }
         _faketonToDraw.transform.position = transform.position;
         int i = 0;
         foreach (SpatialMode mode in _faketonToDraw.spatialModes)
         {
             mode.SwitchToIgnoreLayer();
             mode.transform.position = transform.position;
             mode.transform.localPosition = new Vector3(1.28f * i % 2, 1.28f * Mathf.Ceil(i / 2), 0f);
             i++;
         }

         //Temporary measure since I did not thought about having faketon I would not want to clear
         LevelManager.Instance.RemoveFaketon(_faketonToDraw);
         // C'est moche mais �a passe
         LineRenderer lineRender  = _faketonToDraw.gameObject.GetComponent<LineRenderer>();
         if (lineRender != null)
             lineRender.sortingLayerName = "GameUI";
         _faketonToDraw.DrawFaketon(_radius);

     }
     */
    /*public void OnEnable()
    {
        _faketonToDraw.DrawLine();
    }*/

    private void Awake()
    {
        _faketonToDraw = GetComponent<Faketon>();
        _probaText = GetComponentInChildren<TMP_Text>();
        LevelManager.Instance.RemoveFaketon(_faketonToDraw);
        _allSpatialModes = new SpatialMode[_faketonToDraw.spatialModes.Count];
        _faketonToDraw.spatialModes.CopyTo(_allSpatialModes);
    }

    private void OnDisable()
    {
        Unselect();
        _faketonToDraw.ClearDrawing();
    }

    private void Unselect()
    {
        if (_selectedMode != null)
        {
            _selectedMode.UnSelectSpatialMode();
            _selectedMode = null;
        }
    }

    public void DrawState(State state, int indexOfMode)
    {
        State drawnState = new State(state);
        int numberOfMode = drawnState.numberOfQubits.Length;
        string probaString = (drawnState.proba * 100f).ToString();

        Unselect();
        if (drawnState.phase == 1)
            _probaText.text = "+";
        else
            _probaText.text = "-";
        if (probaString.Length > 5)
            _probaText.text += probaString.Substring(0, 5) + "%";
        else
            _probaText.text += probaString + "%";

        _faketonToDraw.numberOfSpatialModes = numberOfMode;
        _faketonToDraw.spatialModes.Clear();

       
        drawnState.proba = 1;
        for (int i = 0; i < numberOfMode; i++)
        {
            if (indexOfMode == i)
            {
                _selectedMode = _allSpatialModes[i];
                _allSpatialModes[i].SelectSpatialMode();
            }
            _faketonToDraw.spatialModes.Add(_allSpatialModes[i]);
        }
        _faketonToDraw.stateList = new List<State>();
        _faketonToDraw.stateList.Add(state);
        _faketonToDraw.DrawFaketon();
        _faketonToDraw.DrawLine();
    }
}
