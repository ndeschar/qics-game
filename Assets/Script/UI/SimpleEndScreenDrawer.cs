using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Runtime.CompilerServices;

public class SimpleEndScreenDrawer : MonoBehaviour, IResultPrinter
{
    [SerializeField] List<TMP_Text> _textBoxesObj;
    [SerializeField] List<TMP_Text> _perfBoxesObj;
    public void PrintResults(int[] results, float[] comparedPerf)
    {
        int limit;

        gameObject.SetActive(true);
        if (_textBoxesObj == null || results == null || _perfBoxesObj == null)
        {
            Debug.LogError("Unitialized results or textBoxes.");
            return;
        }
        if (_textBoxesObj.Count != results.Length)
            Debug.LogWarning("The number of text boxes is different from the number of results sent");
        limit = Mathf.Min(_textBoxesObj.Count, results.Length);
        for (int i = 0; i < limit; i++)
        {
            _textBoxesObj[i].text = results[i].ToString();
            if (comparedPerf != null)
            {
                int toPrint = Mathf.CeilToInt(comparedPerf[i] * 100);
                _perfBoxesObj[i].text = toPrint.ToString() + "%";
            }
            else
            {
                _perfBoxesObj[i].text = "???" + "%";
            }
        }
    }
}
