using System.Collections;
using System.Collections.Generic;
using UnityEngine;


struct SubmeshData
{
    public int materialToApply;
    public List<int> listOfTri;
}

public class SpatialMode : MonoBehaviour
{
    [SerializeField] GameObject selector;
    private LevelManager _levelManager;
    private const float _precision = 0.0001f;
    private Vector3 _nextLocation;
    private int _numberOfQubits;
    private List<Instrument> _isInsideIntruments;
    private float radius = 0.7f;
    private int slices = 500;
    public Material[] _spatialModesMaterials;
    private MeshRenderer _modeMeshRenderer;
    private MeshFilter _meshFilter;

    public bool ShouldStop { get; set; } = false;
    public bool isDestroyed = false;
    public MeshRenderer ModeMeshRenderer
    {
        get
        {
            if (_modeMeshRenderer == null)
                _modeMeshRenderer = GetComponent<MeshRenderer>();
            return (_modeMeshRenderer);
        }
    }

    public Orientation orientation = Orientation.Right;
    public Faketon owner;

    public int NumberOfQubits
    {
        get { return (_numberOfQubits); }
        set { _numberOfQubits = Mathf.Max(0, value); }
    }
    public LevelManager LevelManagerInst
    {
        get
        {
            if (_levelManager == null)
                _levelManager = LevelManager.Instance;
            return (_levelManager);
        }
    }

    private void Awake()
    {
        _levelManager = LevelManager.Instance;
        _spatialModesMaterials = _levelManager.GetCurrentTheme().materials;
        _nextLocation = transform.localPosition;
        _isInsideIntruments = new List<Instrument>();
        if (!TryGetComponent(out _modeMeshRenderer))
            _modeMeshRenderer = gameObject.AddComponent<MeshRenderer>();
        if (!TryGetComponent(out _meshFilter))
            _meshFilter = gameObject.AddComponent<MeshFilter>();
        transform.SetPositionAndRotation(transform.position, new Quaternion(0f, 180f, 0f, 0f));
    }

    public void ResetNextLocation()
    {
        _nextLocation = transform.localPosition;
    }

    public void Subscribe()
    {
        if (!isDestroyed)
        {
            LevelManagerInst.StartCycle += TriggerCycle;
        }

    }

    public void Unsubscribe()
    {
        LevelManagerInst.StartCycle -= TriggerCycle;
    }

    public void SwitchToIgnoreLayer()
    {
        gameObject.layer = LayerMask.NameToLayer("IgnoringSpatialModes");
    }

    public void SwitchToEnteredLayer()
    {
        gameObject.layer = LayerMask.NameToLayer("NewlyEnteredSpatialMode");
    }

    public void SwitchToCollidingLayer()
    {
        gameObject.layer = LayerMask.NameToLayer("SpatialModes");
    }

    public void AddInstrument(Instrument newlyEnteredInstrument)
    {
        _isInsideIntruments.Add(newlyEnteredInstrument);
    }

    public void RemoveInstrument(Instrument newlyExittedInstrument)
    {
        _isInsideIntruments.Remove(newlyExittedInstrument);
    }

    static bool AreInsideSameInstrument(SpatialMode mode1, SpatialMode mode2)
    {
        foreach (Instrument instru in mode1._isInsideIntruments)
        {
            if (mode2._isInsideIntruments.Contains(instru))
                return (true);
        }
        return (false);
    }

    public bool IsInCollidingLayer()
    {
        return (gameObject.layer == LayerMask.NameToLayer("SpatialModes"));
    }

    public bool IsInIgnoreLayer()
    {
        return (gameObject.layer == LayerMask.NameToLayer("IgnoringSpatialModes"));
    }

    public bool IsInEnteredLayer()
    {
        return (gameObject.layer == LayerMask.NameToLayer("NewlyEnteredSpatialMode"));
    }

    public void TriggerCycle()
    {
        Vector3 velocity;
        Vector3 direction;

        StopAllCoroutines();
        if (ShouldStop)
            return;
        transform.localPosition = _nextLocation;

        direction = LevelManagerInst.levelGrid.cellSize.x * NextDirection();
        velocity = direction / LevelManagerInst.TimeBetweenCycles;
        _nextLocation = transform.localPosition + direction;
        StartCoroutine(Move(velocity, _nextLocation));
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        SpatialMode collidedMode = collision.gameObject.GetComponent<SpatialMode>();

        if (collision.gameObject.tag == "Faketon" && !SpatialMode.AreInsideSameInstrument(this, collidedMode))
        {
           /* bool thisIsNotHere = true;
            bool collidedIsNotHere = true;

            if (this.isDestroyed == false)
            {
                List<SpatialMode> thisModeList = new List<SpatialMode>() { this };
                thisIsNotHere = SpatialMode.MeasureModes(thisModeList, 0, false);
            }
            
            if (collidedMode.isDestroyed == false)
            {
                List<SpatialMode> collidedModeList = new List<SpatialMode>() { collidedMode };
                collidedIsNotHere = SpatialMode.MeasureModes(collidedModeList, 0, false);
            }


            if (!(thisIsNotHere || collidedIsNotHere))*/
                LevelManagerInst.Error(collision.ClosestPoint(transform.position), ErrorType.Collision);
        }
    }

    private IEnumerator Move(Vector3 velocity, Vector3 nextLocation)
    {
        float currentDistance;
        float expectedDistance;
        Vector3 currentVec;
        Vector3 expectedVec;

        currentDistance = (nextLocation - transform.localPosition).magnitude;
        expectedDistance = (nextLocation - transform.localPosition - velocity * Time.fixedDeltaTime).magnitude;
        while (currentDistance >= _precision && expectedDistance < currentDistance)
        {
            currentDistance = (nextLocation - transform.localPosition).magnitude;
            expectedDistance = (nextLocation - transform.localPosition - velocity * Time.fixedDeltaTime).magnitude;
            currentVec = nextLocation - transform.localPosition;
            expectedVec = nextLocation - transform.localPosition - velocity * Time.fixedDeltaTime;
            if (currentVec.x * expectedVec.x < 0 || currentVec.y * expectedVec.y < 0)
                break;
            else
                transform.localPosition += velocity * Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }

        transform.localPosition = nextLocation;
    }

    public void SelectSpatialMode()
    {
        if (selector != null)
            selector.SetActive(true);
    }

    public void UnSelectSpatialMode()
    {
        if (selector != null)
            selector.SetActive(false);
    }

    public static void Destroy(SpatialMode mode)
    {

        if (mode != null && mode.isDestroyed == false)
        {
            //Debug.Log(mode.name);
            mode.isDestroyed = true;
            mode.SwitchToIgnoreLayer();
            mode.Unsubscribe();
            Destroy(mode.gameObject);
        }

    }

    public static void TrashMode(SpatialMode mode)
    {

        Faketon owner = mode.owner;
        List<State>[] newListsState = new List<State>[10];
        float[] maxProba = new float[10];
        int indexOfMode = owner.spatialModes.IndexOf(mode);

        for (int i = 0; i < 10; i++)
        {
            maxProba[i] = 0f;
            newListsState[i] = new List<State>();
        }


        foreach (State state in owner.stateList)
        {
            int numberOfQubit = state.numberOfQubits[indexOfMode];

            State newState = new State();
            newState.proba = state.proba;
            newState.phase = state.phase;
            newState.numberOfQubits = new int[owner.numberOfSpatialModes];
            for (int i = 0; i < owner.numberOfSpatialModes; i++)
            {
                newState.numberOfQubits[i] = state.numberOfQubits[i];
            }
            int prevIndex = Faketon.GetIndexOfState(newListsState[numberOfQubit], newState.numberOfQubits);
            if (prevIndex >= 0)
            {
                State prevState = newListsState[numberOfQubit][prevIndex];
                maxProba[numberOfQubit] -= prevState.proba;
                if (prevState.phase == newState.phase)
                    prevState.proba = Mathf.Pow(Mathf.Sqrt(prevState.proba) + Mathf.Sqrt(newState.proba), 2);
                else
                {
                    if (prevState.proba > newState.proba)
                        prevState.proba = Mathf.Pow(Mathf.Sqrt(prevState.proba) - Mathf.Sqrt(newState.proba), 2);
                    else
                    {
                        prevState.phase = newState.phase;
                        prevState.proba = Mathf.Pow(-Mathf.Sqrt(prevState.proba) + Mathf.Sqrt(newState.proba), 2);
                    }

                }
                if (prevState.proba < 0.05f)
                    newListsState[numberOfQubit].RemoveAt(prevIndex);
                else
                {
                    newListsState[numberOfQubit][prevIndex] = prevState;
                    maxProba[numberOfQubit] += prevState.proba;
                }
            }
            else
            {
                maxProba[numberOfQubit] += newState.proba;
                newListsState[numberOfQubit].Add(newState);
            }


        }

        float dice = Random.Range(0f, 1f);
        float accumulatedProba = 0f;
        for (int i = 0; i < 10; i++)
        {
            accumulatedProba += maxProba[i];
            if (maxProba[i] > 0f && dice <= accumulatedProba)
            {
                //Faketon.Print(newListsState[i]);
                for (int j = 0; j < newListsState[i].Count; j++)
                {
                    newListsState[i][j].proba = newListsState[i][j].proba / maxProba[i];
                }
                owner.stateList = newListsState[i];
                Faketon.TraceSeparateMode(mode);
                //owner.DrawFaketon();
                Faketon.SeparateFaketon(owner);
                owner.CleanEmptyModes();

                break;
            }
        }
        SpatialMode.Destroy(mode);
    }

    public static bool MeasureModes(List<SpatialMode> modes, int numberMeasured, bool destructiveMeasurement)
    {

        Faketon currentOwner;
        List<State>[] newListsState = new List<State>[10];
        float[] maxProba = new float[10];
        List<int> indexOfModes = new List<int>();

        for (int i = 0; i < 10; i++)
        {
            maxProba[i] = 0f;
            newListsState[i] = new List<State>();
        }
        currentOwner = modes[0].owner;
        currentOwner.transform.parent = LevelManager.Instance.levelGrid.transform;
        for (int i = 1; i < modes.Count; i++)
        {
            if (modes[i].owner != currentOwner)
                currentOwner = Faketon.Fuse(currentOwner, modes[i].owner);
        }
        //currentOwner.Print();
        foreach (SpatialMode mode in modes)
        {
            indexOfModes.Add(currentOwner.spatialModes.IndexOf(mode));
        }


        foreach (State state in currentOwner.stateList)
        {
            int numberOfQubit = 0;


            foreach (int index in indexOfModes)
            {
                numberOfQubit += state.numberOfQubits[index];
            }

            State newState = new State();

            newState.proba = state.proba;
            newState.phase = state.phase;
            if (destructiveMeasurement)
            {
                int k = 0;

                newState.numberOfQubits = new int[currentOwner.numberOfSpatialModes - modes.Count];
                for (int i = 0; i < currentOwner.numberOfSpatialModes; i++)
                {
                    if (indexOfModes.Contains(i))
                        k += 1;
                    else
                        newState.numberOfQubits[i - k] = state.numberOfQubits[i];
                }
            }
            else
            {
                newState.numberOfQubits = new int[currentOwner.numberOfSpatialModes];
                for (int i = 0; i < currentOwner.numberOfSpatialModes; i++)
                {
                    newState.numberOfQubits[i] = state.numberOfQubits[i];
                }
            }
            int prevIndex = Faketon.GetIndexOfState(newListsState[numberOfQubit], newState.numberOfQubits);
            if (prevIndex >= 0)
            {
                State prevState = newListsState[numberOfQubit][prevIndex];
                maxProba[numberOfQubit] -= prevState.proba;
                if (prevState.phase == newState.phase)
                    prevState.proba = Mathf.Pow(Mathf.Sqrt(prevState.proba) + Mathf.Sqrt(newState.proba), 2);
                else
                {
                    if (prevState.proba > newState.proba)
                        prevState.proba = Mathf.Pow(Mathf.Sqrt(prevState.proba) - Mathf.Sqrt(newState.proba), 2);
                    else
                    {
                        prevState.phase = newState.phase;
                        prevState.proba = Mathf.Pow(-Mathf.Sqrt(prevState.proba) + Mathf.Sqrt(newState.proba), 2);
                    }

                }
                if (prevState.proba < 0.001f)
                    newListsState[numberOfQubit].RemoveAt(prevIndex);
                else
                {
                    newListsState[numberOfQubit][prevIndex] = prevState;
                    maxProba[numberOfQubit] += prevState.proba;
                }
            }
            else
            {
                maxProba[numberOfQubit] += newState.proba;
                newListsState[numberOfQubit].Add(newState);
            }
        }
        float dice = Random.Range(0f, 1f);
        float accumulatedProba = 0f;
        /*for (int i = 0; i < 10; i++)
        {
            Debug.Log("State if " + i + " was measured");
            Faketon.Print(newListsState[i]);
        }*/
        for (int i = 0; i < 10; i++)
        {
            accumulatedProba += maxProba[i];
            if (maxProba[i] > 0f && dice <= accumulatedProba)
            {
                //Faketon.Print(newListsState[i]);
                for (int j = 0; j < newListsState[i].Count; j++)
                {
                    newListsState[i][j].proba = newListsState[i][j].proba / maxProba[i];
                }
                currentOwner.stateList = newListsState[i];
                if (destructiveMeasurement)
                {
                    currentOwner.numberOfSpatialModes -= modes.Count;
                    while (modes.Count > 0)
                    {
                        SpatialMode modeToDestroy = modes[0];
                        modes.RemoveAt(0);
                        currentOwner.spatialModes.Remove(modeToDestroy);
                        SpatialMode.Destroy(modeToDestroy);
                    }
                    if (currentOwner.numberOfSpatialModes == 0)
                    {
                        Faketon.Destroy(currentOwner);
                        //Debug.Log(i);
                        return (i == numberMeasured);
                    }
                }
                bool isNotDestroyed = currentOwner.CleanEmptyModes();
                if (isNotDestroyed)
                    Faketon.SeparateFaketon(currentOwner);
                //Debug.Log(i);
                return (i == numberMeasured);
            }
        }
        return (false);
    }

    public Orientation RotateClockwise()
    {
        orientation = orientation switch
        {
            Orientation.Left => (Orientation.Up),
            Orientation.Right => (Orientation.Down),
            Orientation.Down => (Orientation.Left),
            Orientation.Up => (Orientation.Right),
            _ => (Orientation.Right),
        };
        return (orientation);
    }

    private Vector3 NextDirection()
    {
        return orientation switch
        {
            Orientation.Left => (Vector3.left),
            Orientation.Right => (Vector3.right),
            Orientation.Down => (Vector3.down),
            Orientation.Up => (Vector3.up),
            _ => (Vector3.zero),
        };
    }

    // Note: base script stolen from https://forum.unity.com/threads/create-a-pie-chart-in-unity.174529/
    public void Draw(List<State> listStates, bool isAmplitude, float wantedRadius = -1)
    {
        var mVertices = new Vector3[slices * 3];
        var mNormals = new Vector3[slices * 3];
        var mUvs = new Vector2[slices * 3];
        var mTriangles = new int[slices * 3];
        var mNormal = new Vector3(0f, 0f, -1f);
        int numberOfPart = listStates.Count;
        _spatialModesMaterials = LevelManagerInst.GetCurrentTheme().materials;
        Material[] arrayOfMaterials = (Material[])_spatialModesMaterials.Clone();
        int numberOfMat = arrayOfMaterials.Length;

        if (wantedRadius > 0f)
            radius = wantedRadius;
        // Calculate sum of data values
        ModeMeshRenderer.materials = arrayOfMaterials;
        // Determine how many triangles in slice
        SubmeshData[] submeshData = new SubmeshData[numberOfMat];
        for (int i = 0; i < numberOfMat; i++)
            submeshData[i].listOfTri = new List<int>();
        int countedSlices = 0;
        int materialToApply = 0;

        for (int i = 0; i < numberOfPart; i++)
        {
            int numOfTris = (int)(listStates[i].proba * slices);

            if (listStates[i].numberOfQubits[0] > 6)
            {
                LevelManagerInst.Error(transform.position, ErrorType.TooMuchFaketon);
                return;
            }
                

            if (listStates[i].phase == 1)
                materialToApply = listStates[i].numberOfQubits[0];
            else
                materialToApply = listStates[i].numberOfQubits[0] + numberOfMat / 2;

            for (int j = countedSlices; j < countedSlices + numOfTris; j++)
            {
                submeshData[materialToApply].listOfTri.Add(j);
            }
            countedSlices += numOfTris;
        }
        if (!isAmplitude)
        {
            while (countedSlices < slices)
            {
                submeshData[materialToApply].listOfTri.Add(countedSlices++);
            }
        }

        // Check that all slices are counted.. if not -> add/sub to/from first slice
        /* int idxOfLargestSlice = 0;
         int largestSliceCount = 0;
         for (int i = 0; i < numberOfPart; i++)
         {
             if (largestSliceCount < slice[i].numberOfTri)
             {
                 idxOfLargestSlice = i;
                 largestSliceCount = slice[i].numberOfTri;
             }
         }*/

        // Check validity for pie chart
        if (countedSlices == 0)
        {
            print("PieChart: Slices == 0");
            return;
        }

        // Adjust largest dataset to get proper slice 
        //slice[idxOfLargestSlice].numberOfTri += slices - countedSlices;

        // Check validity for pie chart data
        /*if (slice[idxOfLargestSlice].numberOfTri <= 0)
        {
            print("PieChart: Largest pie <= 0");
            return;
        }*/

        if (!TryGetComponent(out _meshFilter))
            _meshFilter = gameObject.AddComponent<MeshFilter>();
        Mesh mesh = _meshFilter.mesh;

        mesh.Clear();

        mesh.name = "Pie Chart Mesh";

        // Roration offset (to get star point to "12 o'clock")

        // Calc the points in circle
        float angle;
        float[] x = new float[slices];
        float[] y = new float[slices];

        for (int i = 0; i < slices; i++)
        {
            angle = i * 2f * Mathf.PI / slices;
            x[i] = Mathf.Cos(angle) * radius;
            y[i] = Mathf.Sin(angle) * radius;
        }



        // Generate mesh with slices (vertices and triangles)
        for (int i = 0; i < slices; i++)
        {
            mVertices[i * 3 + 0] = new Vector3(0f, 0f, 0f);
            mVertices[i * 3 + 1] = new Vector3(x[i], y[i], 0f);
            // This will ensure that last vertex = first vertex..
            mVertices[i * 3 + 2] = new Vector3(x[(i + 1) % slices], y[(i + 1) % slices], 0f);

            mNormals[i * 3 + 0] = mNormal;
            mNormals[i * 3 + 1] = mNormal;
            mNormals[i * 3 + 2] = mNormal;

            mUvs[i * 3 + 0] = new Vector2(0f, 0f);
            mUvs[i * 3 + 1] = new Vector2(x[i], y[i]);
            // This will ensure that last uv = first uv..
            mUvs[i * 3 + 2] = new Vector2(x[(i + 1) % slices], y[(i + 1) % slices]);

            mTriangles[i * 3 + 0] = i * 3 + 0;
            mTriangles[i * 3 + 1] = i * 3 + 1;
            mTriangles[i * 3 + 2] = i * 3 + 2;
        }


        // Assign verts, norms, uvs and tris to mesh and calc normals
        mesh.vertices = mVertices;
        mesh.normals = mNormals;
        mesh.uv = mUvs;

        mesh.subMeshCount = numberOfMat;

        int[][] subTris = new int[numberOfMat][];

        for (int i = 0; i < numberOfMat; i++)
        {
            // Every triangle has three veritces..
            subTris[i] = new int[submeshData[i].listOfTri.Count * 3];

            // Add tris to subTris
            int j = 0;
            foreach (int triangle in submeshData[i].listOfTri)
            {
                subTris[i][j * 3 + 0] = mTriangles[triangle * 3 + 0];
                subTris[i][j * 3 + 1] = mTriangles[triangle * 3 + 1];
                subTris[i][j * 3 + 2] = mTriangles[triangle * 3 + 2];
                j += 1;
            }

            mesh.SetTriangles(subTris[i], i);

        }
    }

    public void ClearDraw()
    {
        Mesh mesh = _meshFilter.mesh;
        mesh.Clear();
    }
}
