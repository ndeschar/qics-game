﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class State
{
    public int[] numberOfQubits;
    public float proba;
    public int phase;

    public void Normalize(float maxProba)
    {
        proba = proba / maxProba;
    }

    public void SwitchPhase(int multPhase)
    {
        phase = multPhase * phase;
    }

    public State()
    {

    }

    public State(State initialState, List<int> modesToGet)
    {
        proba = initialState.proba;
        phase = initialState.phase;
        numberOfQubits = new int[modesToGet.Count];
        int k = 0;

        for (int i = 0; i < initialState.numberOfQubits.Length; i++)
        {
            if (modesToGet.Contains(i))
            {
                numberOfQubits[k] = initialState.numberOfQubits[i];
                k = k + 1;
            }
        }
    }

    public State(State initialState)
    {
        proba = initialState.proba;
        phase = initialState.phase;
        numberOfQubits = new int[initialState.numberOfQubits.Length];

        for (int i = 0; i < numberOfQubits.Length; i++)
        {
            numberOfQubits[i] = initialState.numberOfQubits[i];
        }
    }

    public State(State initialState, int modeToGet)
    {
        proba = initialState.proba;
        phase = initialState.phase;
        numberOfQubits = new int[1];

        numberOfQubits[0] = initialState.numberOfQubits[modeToGet];
    }

    public State(State initialState, int modeToGet, int normalization)
    {
        proba = Mathf.Sqrt(initialState.proba / normalization);
        phase = initialState.phase;
        numberOfQubits = new int[1];

        numberOfQubits[0] = initialState.numberOfQubits[modeToGet];
    }
}

public class Faketon : MonoBehaviour
{
    public int numberOfSpatialModes;
    public List<SpatialMode> spatialModes;
    public List<State> stateList;
    private LevelManager _levelManager;
    private LineRenderer _lineRenderer;
    [SerializeField] Material _lineMaterial;
    private Camera _mainCamera;
    //private TicketReceptorMulti _currentReceptor = null;

    public TicketReceptorMulti CurrentReceptor { get; set; }


    private void Awake()
    {
        _levelManager = LevelManager.Instance;
        transform.localPosition = Vector3.zero;
        _levelManager.AddFaketon(this);
        _lineRenderer = GetComponent<LineRenderer>();
        _mainCamera = Camera.main;
        if (_lineRenderer)
            _lineMaterial = _lineRenderer.material;
    }

    public Faketon()
    {
        spatialModes = new List<SpatialMode>();
        stateList = new List<State>();

    }

    public bool CleanEmptyModes()
    {
        List<int> shouldNotBeCleaned = new List<int>();
        List<SpatialMode> modesToDestroy = new List<SpatialMode>();
        List<State> newListState;

        //Debug.Log(name);
        foreach (State state in stateList)
        {
            for (int i = 0; i < state.numberOfQubits.Length; i++)
            {
                if (!shouldNotBeCleaned.Contains(i) && state.numberOfQubits[i] > 0)
                {
                    //Debug.Log(i);
                    shouldNotBeCleaned.Add(i);

                }

            }
            if (shouldNotBeCleaned.Count == state.numberOfQubits.Length)
            {
                //Debug.Log("------------------------");
                return (true);
            }

        }
        //Debug.Log("------------------------");
        if (shouldNotBeCleaned.Count == 0)
        {
            while (spatialModes.Count > 0)
            {
                SpatialMode mode = spatialModes[0];
                spatialModes.RemoveAt(0);
                SpatialMode.Destroy(mode);
            }
            Faketon.DestroyTotally(this);
            return (false);
        }
        newListState = new List<State>();

        for (int i = 0; i < numberOfSpatialModes; i++)
        {
            if (!shouldNotBeCleaned.Contains(i))
                modesToDestroy.Add(spatialModes[i]);
        }
        while (modesToDestroy.Count > 0)
        {
            SpatialMode mode = modesToDestroy[0];
            spatialModes.Remove(mode);
            modesToDestroy.Remove(mode);
            SpatialMode.Destroy(mode);
        }
        numberOfSpatialModes = shouldNotBeCleaned.Count;
        foreach (State state in stateList)
        {
            State stateToAdd = new State(state, shouldNotBeCleaned);
            newListState.Add(stateToAdd);
        }

        stateList = newListState;
        return (true);
    }

    public void Print()
    {
        Debug.Log("Number Of Spatial Mode: " + numberOfSpatialModes);
        foreach (State state in stateList)
        {
            string ket = "|";
            for (int i = 0; i < numberOfSpatialModes; i++)
            {
                ket = ket + state.numberOfQubits[i];
                if (i != numberOfSpatialModes - 1)
                    ket = ket + ",";
                else
                    ket = ket + ">";
            }
            Debug.Log("State: " + state.phase + "*" + state.proba + "*" + ket);
        }
    }

    static public void Print(List<State> sentStateList)
    {
        foreach (State state in sentStateList)
        {
            string ket = "|";
            for (int i = 0; i < state.numberOfQubits.Length; i++)
            {
                ket = ket + state.numberOfQubits[i];
                if (i != state.numberOfQubits.Length - 1)
                    ket = ket + ",";
                else
                    ket = ket + ">";
            }
            Debug.Log("State: " + state.phase + "*" + state.proba + "*" + ket);
        }
    }

    public override string ToString()
    {
        string finalKet = "";
        bool firstStateFlag = true;

        foreach (State state in stateList)
        {
            if (firstStateFlag)
            {
                firstStateFlag = false;
                if (state.phase == -1)
                    finalKet += "-";
            }
            else
            {
                if (state.phase == -1)
                    finalKet += " - ";
                else
                    finalKet += " + ";
            }
            finalKet += state.proba.ToString() + "*";
            finalKet += "|";
            for (int i = 0; i < numberOfSpatialModes; i++)
            {
                finalKet += state.numberOfQubits[i].ToString();
                if (i != numberOfSpatialModes - 1)
                    finalKet += ",";
                else
                    finalKet += ">";
            }
        }
        return (finalKet);
    }

    public static Faketon Fuse(Faketon fakeOne, Faketon fakeTwo)
    {
        Faketon fakeChild;

        var faketonGameObject = new GameObject("FaketonFused", typeof(Faketon), typeof(LineRenderer));
        faketonGameObject.transform.SetParent(fakeOne.transform.parent, true);
        faketonGameObject.GetComponent<LineRenderer>().material = fakeOne._lineMaterial;
        fakeChild = faketonGameObject.GetComponent<Faketon>();
        fakeChild._lineRenderer = faketonGameObject.GetComponent<LineRenderer>();
        fakeChild._lineMaterial = fakeOne._lineMaterial;
        fakeChild.transform.position = fakeOne.transform.position;

        fakeChild.numberOfSpatialModes = fakeOne.numberOfSpatialModes + fakeTwo.numberOfSpatialModes;
        foreach (SpatialMode mode in fakeOne.spatialModes)
        {
            fakeChild.spatialModes.Add(mode);
            mode.owner = fakeChild;
            mode.transform.SetParent(fakeChild.transform, true);
            //mode.transform.position = Vector3.zero;
        }
        foreach (SpatialMode mode in fakeTwo.spatialModes)
        {
            fakeChild.spatialModes.Add(mode);
            mode.owner = fakeChild;
            mode.transform.SetParent(fakeChild.transform, true);
            // mode.transform.position = Vector3.zero;
        }
        foreach (State state in fakeOne.stateList)
        {
            foreach (State state2 in fakeTwo.stateList)
            {
                State newState = new State();

                newState.proba = state.proba * state2.proba;
                newState.phase = state.phase * state2.phase;
                newState.numberOfQubits = new int[fakeChild.numberOfSpatialModes];
                for (int i = 0; i < fakeOne.numberOfSpatialModes; i++)
                {
                    newState.numberOfQubits[i] = state.numberOfQubits[i];
                }
                for (int i = 0; i < fakeTwo.numberOfSpatialModes; i++)
                {
                    newState.numberOfQubits[i + fakeOne.numberOfSpatialModes] = state2.numberOfQubits[i];
                }
                fakeChild.stateList.Add(newState);
            }
        }
        Destroy(fakeOne);
        Destroy(fakeTwo);
        return (fakeChild);
    }

    public static bool IsEqual(Faketon fakeOne, Faketon fakeTwo)
    {
        int phaseCorrection = 1;
        int index;
        //for now I put it here we may to put it in an easily accessible parameter
        float precision = 0.005f;

        if (fakeOne.numberOfSpatialModes != fakeTwo.numberOfSpatialModes)
            return (false);
        if (fakeOne.stateList.Count != fakeTwo.stateList.Count)
            return (false);
        index = Faketon.GetIndexOfState(fakeTwo.stateList, fakeOne.stateList[0].numberOfQubits);
        if (index == -1 || fakeOne.stateList[0].proba < fakeTwo.stateList[index].proba - precision 
                         || fakeOne.stateList[0].proba > fakeTwo.stateList[index].proba + precision)
            return (false);
        else if (fakeOne.stateList[0].phase != fakeTwo.stateList[index].phase)
        {
            phaseCorrection = -1;
        }
            

        foreach (State state in fakeOne.stateList)
        {
            index = Faketon.GetIndexOfState(fakeTwo.stateList, state.numberOfQubits);
            if (index == -1)
                return (false);
            if (state.proba < fakeTwo.stateList[index].proba - precision
                || state.proba > fakeTwo.stateList[index].proba + precision
                || state.phase * phaseCorrection != fakeTwo.stateList[index].phase)
            {
                return (false);
            }

        }
        return (true);
    }

    public static int GetIndexOfState(List<State> stateList, int[] mode)
    {
        if (stateList.Count == 0)
            return (-1);
        int size = stateList[0].numberOfQubits.Length;
        int size2 = mode.Length;

        if (size != size2)
        {
            return (-1);
        }

        foreach (State state in stateList)
        {
            int i = 0;

            while (i < size && state.numberOfQubits[i] == mode[i])
                i++;
            if (i == size)
                return (stateList.IndexOf(state));
        }
        return (-1);
    }

    public static State GetFirstIndexOfSubState(List<State> stateList, int[] mode, int[] indexes)
    {
        if (stateList.Count == 0)
            return (null);
        int size = stateList[0].numberOfQubits.Length;
        int size2 = mode.Length;

        foreach (State state in stateList)
        {
            for (int i = 0; i < size2; i++)
            {
                if (state.numberOfQubits[indexes[i]] != mode[i])
                    break;
            }
        }
        return (null);
    }

    public static void TraceSeparateMode(SpatialMode modeToTrace)
    {
        Faketon faketon = modeToTrace.owner;
        int indexOfSpatialMode;
        List<State> newListState = new List<State>();

        if (faketon.numberOfSpatialModes <= 1)
        {
            SpatialMode.Destroy(modeToTrace);
            Faketon.Destroy(faketon);
            return;
        }
        faketon.numberOfSpatialModes -= 1;
        indexOfSpatialMode = faketon.spatialModes.IndexOf(modeToTrace);
        foreach (State state in faketon.stateList)
        {
            State newState = new State();
            int k = 0;

            newState.phase = state.phase;
            newState.proba = state.proba;
            newState.numberOfQubits = new int[faketon.numberOfSpatialModes];
            for (int i = 0; i <= faketon.numberOfSpatialModes; i++)
            {
                if (i == indexOfSpatialMode)
                    k = -1;
                else
                    newState.numberOfQubits[i + k] = state.numberOfQubits[i];
            }

            newListState.Add(newState);
        }
        faketon.spatialModes.RemoveAt(indexOfSpatialMode);
        faketon.stateList = newListState;
        SpatialMode.Destroy(modeToTrace);
    }


    public static void TraceOverMode(SpatialMode modeToTrace)
    {
        Faketon faketon = modeToTrace.owner;
        int indexOfSpatialMode;
        List<State> newListState = new List<State>();

        if (faketon.numberOfSpatialModes <= 1)
        {
            Destroy(modeToTrace);
            Destroy(faketon);
            return;
        }
        faketon.numberOfSpatialModes -= 1;
        indexOfSpatialMode = faketon.spatialModes.IndexOf(modeToTrace);
        foreach (State state in faketon.stateList)
        {
            State newState = new State();
            int k = 0;

            newState.phase = state.phase;
            newState.proba = state.proba;
            newState.numberOfQubits = new int[faketon.numberOfSpatialModes];
            for (int i = 0; i <= faketon.numberOfSpatialModes; i++)
            {
                if (i == indexOfSpatialMode)
                    k = -1;
                else
                {
                    newState.numberOfQubits[i + k] = state.numberOfQubits[i];
                }
            }

            int prevIndex = Faketon.GetIndexOfState(newListState, newState.numberOfQubits);
            if (prevIndex >= 0)
            {
                State prevState = newListState[prevIndex];
                if (prevState.phase == newState.phase)
                    prevState.proba = Mathf.Pow(Mathf.Sqrt(prevState.proba) + Mathf.Sqrt(newState.proba), 2);
                else
                {
                    if (prevState.proba > newState.proba)
                        prevState.proba = Mathf.Pow(Mathf.Sqrt(prevState.proba) - Mathf.Sqrt(newState.proba), 2);
                    else
                    {
                        prevState.phase = newState.phase;
                        prevState.proba = Mathf.Pow(-Mathf.Sqrt(prevState.proba) + Mathf.Sqrt(newState.proba), 2);
                    }

                }
                if (prevState.proba < 0.001f)
                    newListState.RemoveAt(prevIndex);
                else
                    newListState[prevIndex] = prevState;
            }
            else
                newListState.Add(newState);
        }
        faketon.spatialModes.RemoveAt(indexOfSpatialMode);
        faketon.stateList = newListState;
        SpatialMode.Destroy(modeToTrace);
    }

    public static void TraceOverModes(List<SpatialMode> modesToTrace)
    {
        Faketon faketon = modesToTrace[0].owner;
        int[] indexOfSpatialMode = new int[modesToTrace.Count];
        List<State> newListState = new List<State>();

        if (faketon.numberOfSpatialModes <= modesToTrace.Count)
        {
            foreach (SpatialMode mode in modesToTrace)
                Destroy(mode);
            Destroy(faketon);
            return;
        }
        faketon.numberOfSpatialModes -= modesToTrace.Count;
        for (int i = 0; i < modesToTrace.Count; i++)
            indexOfSpatialMode[i] = faketon.spatialModes.IndexOf(modesToTrace[i]);
        foreach (State state in faketon.stateList)
        {
            State newState = new State();
            int k = 0;

            newState.phase = state.phase;
            newState.proba = state.proba;
            newState.numberOfQubits = new int[faketon.numberOfSpatialModes];
            for (int i = 0; i < faketon.numberOfSpatialModes + modesToTrace.Count; i++)
            {
                if (Array.IndexOf(indexOfSpatialMode, i) >= 0)
                    k -= 1;
                else
                {
                    newState.numberOfQubits[i + k] = state.numberOfQubits[i];
                }
            }

            int prevIndex = Faketon.GetIndexOfState(newListState, newState.numberOfQubits);
            if (prevIndex >= 0)
            {
                State prevState = newListState[prevIndex];
                if (prevState.phase == newState.phase)
                    prevState.proba = Mathf.Pow(Mathf.Sqrt(prevState.proba) + Mathf.Sqrt(newState.proba), 2);
                else
                {
                    if (prevState.proba > newState.proba)
                        prevState.proba = Mathf.Pow(Mathf.Sqrt(prevState.proba) - Mathf.Sqrt(newState.proba), 2);
                    else
                    {
                        prevState.phase = newState.phase;
                        prevState.proba = Mathf.Pow(-Mathf.Sqrt(prevState.proba) + Mathf.Sqrt(newState.proba), 2);
                    }

                }
                if (prevState.proba < 0.001f)
                    newListState.RemoveAt(prevIndex);
                else
                    newListState[prevIndex] = prevState;
            }
            else
                newListState.Add(newState);
        }
        for (int i = 0; i < modesToTrace.Count; i++)
        {
            faketon.spatialModes.Remove(modesToTrace[i]);
            Destroy(modesToTrace[i]);
        }
        faketon.stateList = newListState;

    }


    public static List<State> NormalizeLinearCombination(List<State> linearCombination)
    {
        List<State> normalizedListState = new List<State>();

        float maxProba = 0f;

        foreach (State state in linearCombination)
        {
            maxProba += state.proba;
        }

        foreach (State state in linearCombination)
        {
            State stateToAdd = new State();
            stateToAdd.proba = state.proba / maxProba;
            stateToAdd.phase = state.phase;
            stateToAdd.numberOfQubits = state.numberOfQubits;
            normalizedListState.Add(stateToAdd);
        }

        return (normalizedListState);
    }


    public static void Destroy(Faketon faketon)
    {
        faketon._levelManager.RemoveFaketon(faketon);
        Destroy(faketon.gameObject);
    }

    public static void DestroyTotally(Faketon faketon)
    {
        faketon.numberOfSpatialModes = faketon.spatialModes.Count;
        while (faketon.numberOfSpatialModes > 0)
        {
            SpatialMode toDestroy = faketon.spatialModes[0];
            faketon.numberOfSpatialModes -= 1;
            SpatialMode.Destroy(toDestroy);
            faketon.spatialModes.RemoveAt(0);
        }
        faketon._levelManager.RemoveFaketon(faketon);
        Destroy(faketon.gameObject);
    }

    #region Separability Check

    private static List<State> GetSubStates(List<int> indexesOfModes, List<int> numberOfQubitsInMode, List<State> linearCombination)
    {
        List<State> subState = new List<State>();

        foreach (State state in linearCombination)
        {
            int i = 0;
            while (i < indexesOfModes.Count && state.numberOfQubits[indexesOfModes[i]] == numberOfQubitsInMode[i])
            {
                i = i + 1;
            }
            if (i == indexesOfModes.Count)
                subState.Add(state);
        }

        return (subState);
    }

    private static State GetSubState(List<int> indexesOfModes, List<int> numberOfQubitsInMode, List<State> linearCombination)
    {
        foreach (State state in linearCombination)
        {
            int i = 0;
            while (i < indexesOfModes.Count && state.numberOfQubits[indexesOfModes[i]] == numberOfQubitsInMode[i])
            {
                i = i + 1;
            }
            if (i == indexesOfModes.Count)
                return (state);
        }

        return (null);
    }

    private static float? AreSubStatesColinear(List<State> vectorOne, List<State> vectorTwo, List<int> indexesOfModes)
    {
        float? factor = null;
        List<State> vectorOneCopy = new List<State>(vectorOne);
        List<State> vectorTwoCopy = new List<State>(vectorTwo);

        while (vectorOneCopy.Count > 0)
        {
            State currentState = vectorOneCopy[0];
            List<int> numberOfQubitsInMode = new List<int>();
            State matchingState;

            foreach (int index in indexesOfModes)
            {
                numberOfQubitsInMode.Add(currentState.numberOfQubits[index]);
            }
            matchingState = GetSubState(indexesOfModes, numberOfQubitsInMode, vectorTwoCopy);
            if (matchingState == null)
                return (null);
            else
            {
                if (factor == null)
                    factor = currentState.proba * currentState.phase / (matchingState.proba * matchingState.phase);
                else if (currentState.proba * currentState.phase / (matchingState.proba * matchingState.phase) != factor)
                    return (null);
            }
            vectorOneCopy.RemoveAt(0);
            vectorTwoCopy.Remove(matchingState);
        }

        return (factor);
    }

    private static List<State>[] SeparateStates(List<List<State>> inputs, List<float?> factors, List<int> indexesOfModesOne, List<int> indexesOfModesTwo)
    {
        List<State>[] statesArray = new List<State>[2];

        statesArray[0] = new List<State>();
        statesArray[1] = new List<State>();
        for (int i = 0; i < inputs.Count; i++)
        {
            State stateToAdd = new State();
            int j = 0;

            stateToAdd.proba = Mathf.Abs(factors[i].Value);
            stateToAdd.phase = (int)Mathf.Sign(factors[i].Value);
            stateToAdd.numberOfQubits = new int[indexesOfModesOne.Count];
            foreach (int index in indexesOfModesOne)
            {
                stateToAdd.numberOfQubits[j] = (inputs[i])[0].numberOfQubits[index];
                j = j + 1;
            }
            statesArray[0].Add(stateToAdd);
        }
        foreach (State state in inputs[0])
        {
            State stateToAdd = new State();
            int j = 0;

            stateToAdd.proba = state.proba;
            stateToAdd.phase = state.phase;
            stateToAdd.numberOfQubits = new int[indexesOfModesTwo.Count];
            foreach (int index in indexesOfModesTwo)
            {
                stateToAdd.numberOfQubits[j] = state.numberOfQubits[index];
                j = j + 1;
            }
            statesArray[1].Add(stateToAdd);
        }

        statesArray[0] = Faketon.NormalizeLinearCombination(statesArray[0]);
        statesArray[1] = Faketon.NormalizeLinearCombination(statesArray[1]);

        return (statesArray);
    }

    public static List<State>[] TryToSeparate(List<int> indexesOfModes, List<State> stateList)
    {
        List<int> oppositeIndexes = new List<int>();
        List<List<State>> separateState = new List<List<State>>();
        List<float?> factors = new List<float?>();
        List<State> stateListCopy = new List<State>(stateList);

        for (int i = 0; i < stateListCopy[0].numberOfQubits.Length; i++)
        {
            if (!indexesOfModes.Contains(i))
                oppositeIndexes.Add(i);
        }

        while (stateListCopy.Count > 0)
        {

            State firstState = stateListCopy[0];
            List<int> numbersInMode = new List<int>();
            List<State> newList;


            for (int i = 0; i < indexesOfModes.Count; i++)
                numbersInMode.Add(firstState.numberOfQubits[indexesOfModes[i]]);
            newList = GetSubStates(indexesOfModes, numbersInMode, stateListCopy);
            separateState.Add(newList);
            if (separateState.Count == 1)
            {
                if (stateListCopy.Count % newList.Count != 0)
                    return (null);
                factors.Add(1f);
            }
            else
            {
                float? newFactor = AreSubStatesColinear(separateState[0], newList, oppositeIndexes);
                if (newFactor == null)
                    return (null);
                factors.Add(newFactor);
            }
            foreach (State state in newList)
                stateListCopy.Remove(state);
        }
        /*Debug.Log("What is Sent to SeparateState");
        foreach (List<State> listOfState in separateState)
        {
            Print(listOfState);
            Debug.Log("+++++++++++++");
        }
        Debug.Log("---------------------------");*/
        return (Faketon.SeparateStates(separateState, factors, indexesOfModes, oppositeIndexes));
    }

    private static List<List<int>> GetAllBirepartion(List<int> setElem)
    {

        List<List<int>> response;
        List<List<int>> temp = new List<List<int>>();
        int curElem;

        if (setElem.Count == 1)
        {
            response = new List<List<int>>();
            return (response);
        }
        curElem = setElem[0];
        setElem.RemoveAt(0);
        response = GetAllBirepartion(setElem);
        List<int> single = new List<int>();
        single.Add(curElem);
        temp.Add(single);
        foreach (List<int> birepartion in response)
        {
            List<int> newBiRepartition = new List<int>(birepartion);
            newBiRepartition.Add(curElem);
            temp.Add(newBiRepartition);
        }
        foreach (List<int> elem in temp)
        {
            response.Add(elem);
        }
        return (response);
    }

    public static void SeparateFaketon(Faketon faketonToSeparate)
    {
        List<int> setElem = new List<int>();

        for (int i = 0; i < faketonToSeparate.numberOfSpatialModes; i++)
        {
            setElem.Add(i);
        }
        SeparateFaketon(faketonToSeparate, setElem);
    }

    public static void SeparateFaketon(Faketon faketonToSeparate, List<int> setElem)
    {
        List<List<int>> birepartitionList = GetAllBirepartion(setElem);
        bool isSeparated = false;


        //DEBUG
        /*Debug.Log("faketon A séparer: " + faketonToSeparate.ToString());
        Debug.Log("Birepartition testées :");
        foreach (List<int> list in birepartitionList)
        {
            string listStr = "{";
            foreach (int integer in list)
            {
                listStr = listStr + integer.ToString() + ",";
            }
            listStr = listStr + "}";
            Debug.Log(listStr);
        }
        Debug.Log("--------------------------------------------");*/
        foreach (List<int> listOfIndex in birepartitionList)
        {
            /* Debug.Log("Birepartition testée :");
            string listStr = "{";
            foreach (int integer in listOfIndex)
            {
                listStr = listStr + integer.ToString() + ",";
            }
            listStr = listStr + "}";
            Debug.Log(listStr);*/
            List<State>[] separation = TryToSeparate(listOfIndex, faketonToSeparate.stateList);
            if (separation != null)
            {
                /*Debug.Log("Separable");
                Debug.Log("First Sub State");
                Faketon.Print(separation[0]);
                Debug.Log("Second Sub State");
                Print(separation[1]);*/
                Faketon fakeChildOne;
                Faketon fakeChildTwo;
                var faketonOneGameObject = new GameObject("FaketonOne", typeof(Faketon), typeof(LineRenderer));
                var faketonTwoGameObject = new GameObject("FaketonTwo", typeof(Faketon), typeof(LineRenderer));
                int i = 0;
                isSeparated = true;
                faketonOneGameObject.GetComponent<LineRenderer>().material = faketonToSeparate._lineMaterial;
                faketonTwoGameObject.GetComponent<LineRenderer>().material = faketonToSeparate._lineMaterial;


                faketonOneGameObject.transform.position = faketonToSeparate.transform.position;
                faketonTwoGameObject.transform.position = faketonToSeparate.transform.position;
                faketonOneGameObject.transform.SetParent(faketonToSeparate.transform.parent, true);
                faketonTwoGameObject.transform.SetParent(faketonToSeparate.transform.parent, true);
                fakeChildOne = faketonOneGameObject.GetComponent<Faketon>();
                fakeChildOne._lineMaterial = faketonToSeparate._lineMaterial;
                fakeChildOne._lineRenderer = faketonOneGameObject.GetComponent<LineRenderer>();
                fakeChildTwo = faketonTwoGameObject.GetComponent<Faketon>();
                fakeChildTwo._lineMaterial = faketonToSeparate._lineMaterial;
                fakeChildTwo._lineRenderer = faketonTwoGameObject.GetComponent<LineRenderer>();
                fakeChildOne.numberOfSpatialModes = listOfIndex.Count;
                fakeChildTwo.numberOfSpatialModes = faketonToSeparate.numberOfSpatialModes - listOfIndex.Count;
                foreach (SpatialMode mode in faketonToSeparate.spatialModes)
                {
                    if (listOfIndex.Contains(i))
                    {
                        fakeChildOne.spatialModes.Add(mode);
                        mode.owner = fakeChildOne;
                        mode.transform.SetParent(fakeChildOne.transform, true);
                    }
                    else
                    {
                        fakeChildTwo.spatialModes.Add(mode);
                        mode.owner = fakeChildTwo;
                        mode.transform.SetParent(fakeChildTwo.transform, true);
                    }
                    i = i + 1;
                }
                fakeChildOne.stateList = separation[0];
                fakeChildTwo.stateList = separation[1];
                fakeChildOne.DrawFaketon();
                fakeChildTwo.DrawFaketon();
                Destroy(faketonToSeparate);
                if (fakeChildOne.CleanEmptyModes())
                    SeparateFaketon(fakeChildOne);
                if (fakeChildTwo.CleanEmptyModes())
                    SeparateFaketon(fakeChildTwo);
                break;
            }
            else
            {
                //Debug.Log("Non-Separable");
            }
        }
        if (!isSeparated)
        {
            faketonToSeparate.CheckIfOverLinearized();
            faketonToSeparate.stateList = Faketon.NormalizeLinearCombination(faketonToSeparate.stateList);
            if (faketonToSeparate.numberOfSpatialModes != 0)
                faketonToSeparate.DrawFaketon();
        }

    }

    public void CheckIfOverLinearized()
    {
        if (stateList.Count > _levelManager.MaxLenghtOfLinearCombination)
        {
            _levelManager.Error(spatialModes[spatialModes.Count - 1].transform.position, ErrorType.TooMuchStates);
        }
    }

    #endregion

    #region Drawing Area

    public void DrawLine()
    {
        if (_lineRenderer)
        {
            _lineRenderer.positionCount = numberOfSpatialModes;
            _lineRenderer.widthMultiplier = _originalWidthLine * _mainCamera.orthographicSize / _originalCamSize;
            for (int i = 0; i < numberOfSpatialModes; i++)
            {
                _lineRenderer.SetPosition(i, new Vector3(spatialModes[i].transform.position.x, spatialModes[i].transform.position.y, spatialModes[i].transform.position.z + 0.1f));
                //_lineRenderer.SetPosition(i, spatialModes[i].transform.localPosition);
            }
        }

    }

    private float _originalWidthLine = 0.1f;
    private float _originalCamSize = 5f;

    public void DrawFaketon(float radius = -1f)
    {
        List<State>[] arrayToDraw = new List<State>[numberOfSpatialModes];
        int numberOfStates = stateList.Count;
        bool isAmplitude = false; // remains of a bad idea to represent states as amplitude

        for (int i = 0; i < numberOfSpatialModes; i++)
        {
            arrayToDraw[i] = new List<State>();
        }
        foreach (State state in stateList)
        {
            for (int i = 0; i < numberOfSpatialModes; i++)
            {
                State spatialModeState;
                if (isAmplitude)
                    spatialModeState = new State(state, i, numberOfStates);
                else
                {
                    spatialModeState = new State(state, i);
                }
                arrayToDraw[i].Add(spatialModeState);
            }
        }
        if (radius > 0f)
        {
            for (int i = 0; i < numberOfSpatialModes; i++)
            {
                spatialModes[i].Draw(arrayToDraw[i], isAmplitude, radius);
            }
        }
        else
        {
            for (int i = 0; i < numberOfSpatialModes; i++)
            {
                spatialModes[i].Draw(arrayToDraw[i], isAmplitude);
            }
        }
    }




    public void ClearDrawing()
    {
        for (int i = 0; i < numberOfSpatialModes; i++)
        {
            spatialModes[i].ClearDraw();
        }
    }

    #endregion

    public void TriggerCycle()
    {

    }

}
