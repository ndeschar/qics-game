using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITriggerClear
{
    public void SubscribeClear();
    public void UnsubscribeClear();
    public void TriggerClear();
}
