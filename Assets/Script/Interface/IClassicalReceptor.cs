using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public interface IClassicalReceptor
{

    public void SetIsControlled(bool isControlled);

    public void ReceiveSignal(bool signal);

}
