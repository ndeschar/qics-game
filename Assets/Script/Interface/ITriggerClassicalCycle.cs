using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITriggerClassicalCycle
{
    public void TriggerClassicalCycle();
    public void SubscribeClassicalCycle();
    public void UnsubscribeClassicalCycle();

}
