using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface INeedHistoryManager
{
    public void SetHistoryManager(IHistoryManager manager);
}
