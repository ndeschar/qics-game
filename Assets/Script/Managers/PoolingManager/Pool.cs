using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour
{
    [SerializeField] int _initialQuantity;
    [SerializeField] int _maxQuantity;
    [SerializeField] int _dangerQuantity;
    [SerializeField] float _relaxationTime;
    [SerializeField] GameObject _prefab;

    public void InitPool()
    {
        if (_prefab == null)
        {
            Debug.LogError("No prefab to instantiate");
            return;
        }
    }

}
