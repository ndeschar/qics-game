using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultDisplayer : MonoBehaviour, IDisplayerManager
{
    [SerializeField] List<StateDrawerUI> _listOfStateDrawer;
    [SerializeField] GameObject _background;
    private int _numberOfDisplayedState = 10;
    private List<SpatialMode> _selectedSpatialMode;

    public void Init()
    {
        _numberOfDisplayedState = _listOfStateDrawer.Count;
        RemoveFromScreen();
        if (_numberOfDisplayedState == 0)
        {
            Debug.LogWarning("No state displayer");
        }

        _selectedSpatialMode = new List<SpatialMode>();
    }

    public void RemoveFromScreen()
    {
        if (_background)
            _background.SetActive(false);
        foreach (StateDrawerUI drawer in _listOfStateDrawer)
        {
            drawer.gameObject.SetActive(false);
        }
    }

    public void DisplayFaketon(SpatialMode mode)
    {
        List<State> _mostProbableStates = new List<State>();
        Faketon faketon = mode.owner;
        int indexOfMode = faketon.spatialModes.IndexOf(mode);

        foreach (State state in faketon.stateList)
        {
            float curProba = state.proba;
            int positionToAdd = 0;

            foreach (State registeredState in _mostProbableStates)
            {
                if (curProba > registeredState.proba)
                    break;
                else
                    positionToAdd = positionToAdd + 1;
            }
            if (positionToAdd < _numberOfDisplayedState)
            {
                if (_mostProbableStates.Count == _numberOfDisplayedState)
                {
                    _mostProbableStates.RemoveAt(_numberOfDisplayedState - 1);
                }
                _mostProbableStates.Insert(positionToAdd, state);
            }
        }

        int i = 0;
        int numberOfStateToEffectivelyDisplay = _mostProbableStates.Count;
        int numberOfModeToDisplay = faketon.numberOfSpatialModes;
        if (_background != null)
        {
            _background.SetActive(true);
            _background.transform.localScale = new Vector3(3.6f + 0.75f * numberOfModeToDisplay, 
                            numberOfStateToEffectivelyDisplay * 0.75f + 0.25f, _background.transform.localScale.z);

        }
        foreach (State stateToDisplay in _mostProbableStates)
        {
            _listOfStateDrawer[i].transform.localPosition = ((numberOfStateToEffectivelyDisplay - 1) / 2f - i) * 0.75f * Vector3.up 
                                                        + 20f * Vector3.forward;
            _listOfStateDrawer[i].gameObject.SetActive(true);
            _listOfStateDrawer[i].DrawState(stateToDisplay, indexOfMode);
            i = i + 1;
        }
    }

    public void Scrolling(int direction)
    {

    }
}
