using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine;

public interface IDisplayerManager
{
    public void RemoveFromScreen();

    public void DisplayFaketon(SpatialMode mode);

    public void Scrolling(int direction);

    public void Init();

}
