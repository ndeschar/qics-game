using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine;

public interface IInputManager : INeedGridHandler, INeedDialogueManager, INeedDisplayManager, INeedHistoryManager
{
    //This method should be used to bind each method to the associated input event
    public void Init();
    public void OnPosition(InputValue data);
    public void OnClickPlayer(InputValue data);
    public void OnDelete(InputValue data);
    public void OnRotate(InputValue data);
    public void OnRotateButton();
    public void OnNegate(InputValue data);

    //dirty
    public void UnselectSpatialMode();

    public void OnZoom(InputValue data);
    public bool IsSelected(GameObject gameObject);
    public void ClearSelection();
    public void AddToSelection(GameObject gameObject);
    public void RemoveFromSelection(GameObject gameObject);
}
