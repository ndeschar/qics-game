using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine;
using System.Linq;
using System;


public class DebugInputManager : MonoBehaviour, IInputManager
{
    //Ideally, to remove at the end
    private LevelManager _levelManager;


    private Vector2 _mousePosition;
    private Camera _mainCamera;

    private Transform _draggedObject;
    private IDraggable _draggableComponent;
    private bool _isDragging = false;
    private IList<GameObject> _selected = new List<GameObject>();

    private IDisplayerManager _displayManager;
    private GridHandler _gridHandler;
    private IDialogueManager _dialogueManager;
    private IHistoryManager _historyManager;

    public void Init()
    {
        _mainCamera = Camera.main;
        _levelManager = LevelManager.Instance;
        _levelManager.QueryGridHandler(this);
        _levelManager.QueryDialogueManager(this);
    }

    public void OnZoom(InputValue data)
    {
        Debug.Log(data.Get<float>());
    }
    public void OnPosition(InputValue data)
    {
        _mousePosition = data.Get<Vector2>();
        if (_isDragging)
        {
            Vector3 worldPos = _mainCamera.ScreenToWorldPoint(_mousePosition);
            _draggableComponent.DragObject(new Vector3(worldPos.x, worldPos.y, _draggedObject.position.z), _mousePosition);
        }
    }

    public void OnClickPlayer(InputValue data)
    {
        Ray inputRay = _mainCamera.ScreenPointToRay(_mousePosition);
        RaycastHit2D hitResults = new RaycastHit2D();

        if (data.isPressed && _levelManager.IsLevelStateUI())
        {
            Debug.Log("On est en mode UI");
            return;
        }

        hitResults = Physics2D.Raycast(inputRay.origin, inputRay.direction, 1000f);

        Debug.Log("On a touch� : " + hitResults.transform.name + " qui est du layer " + LayerMask.LayerToName(hitResults.transform.gameObject.layer)); 


    }

    //Multple selection commented for now
    /// <summary>
    /// Same as OnClickPlayer but for multi-select
    /// </summary>
    /// <param name="data"></param>
    /*public void OnCtrlClickPlayer(InputValue data)
    {
        if (!_levelManager.IsLevelStateCleared())
            return;
        if (data.isPressed)
        {
            // If the object clicked on is selectable (now: only Instruments are), select it. Else, deselect the current selection.
            Ray inputRay = _mainCamera.ScreenPointToRay(_mousePosition);
            RaycastHit2D hitResults = Physics2D.Raycast(inputRay.origin, inputRay.direction, 100f, 1 << 3);
            if (hitResults.transform != null)
            {
                if (hitResults.transform.gameObject.TryGetComponent(out Instrument instrument))
                {
                    instrument.selector.SetActive(true);
                    //instrument.selector.GetComponent<SpriteRenderer>().enabled = true;
                    if (!_selected.Contains(instrument.gameObject))
                    {
                        _selected.Add(instrument.gameObject);
                    }
                }

            }
            else
            {
                ClearSelection();
            }
        }
    }*/



    /// <summary>
    /// Is called when the Delete (suppr for now) key is pressed. It removes every selected object which can be modified.
    /// </summary>
    /// <param name="data"></param>
    public void OnDelete(InputValue data)
    {
        List<GameObject> newSelected = new List<GameObject>();

        if (data.isPressed)
        {
            foreach (GameObject obj in _selected)
            {
                // This part should be refactored. one interface IDestroyable, we check if interface is here and call the Destroy function
                Instrument associatedInstrument = obj.GetComponent<Instrument>();
                if (associatedInstrument != null)
                {
                    
                    if (associatedInstrument.isDraggable)
                    {
                      //  _gridHandler.FreeCell(obj.transform.position);
                      //  if (!associatedInstrument.associatedTicket.isUnlimited)
                      // {
                      //      associatedInstrument.associatedTicket.remainingCopies += 1;
                      //      associatedInstrument.associatedTicket.spriteRenderer.color = Color.white;
                      //  }

                        Instrument.Destroy(associatedInstrument);
                    }
                    else
                        newSelected.Add(obj);
                }
                else
                {
                    var classicalThread = obj.GetComponent<ClassicalThread>();
                    ClassicalThread.Destroy(classicalThread);
                }
            }
            _selected = newSelected;
            _isDragging = false;
        }
    }

    /// <summary>
    /// Is called when the Rotate (R for now) key is pressed. It rotates every selected object which can be modified.
    /// </summary>
    /// <param name="data"></param>
    public void OnRotate(InputValue data)
    {
        if (data.isPressed && !_levelManager.IsInClassicalMode)
        {
            foreach (GameObject obj in _selected)
            {
                Instrument instrument = obj.GetComponent<Instrument>();
                Orientation newOrientation = instrument.RotateClockwise();
                _gridHandler.ChangeDataOrientation(obj.transform.position, newOrientation);
            }
        }
    }

    public void OnRotateButton()
    {

    }
    public void OnNegate(InputValue data)
    {
        if (data.isPressed && _levelManager.IsInClassicalMode)
        {
            foreach (GameObject obj in _selected)
            {
                var classicalThread = obj.GetComponent<ClassicalThread>();
                classicalThread.SwitchNotMode();

            }
        }
    }

    public void UnselectSpatialMode()
    {

    }

    public void ClearSelection()
    {
        foreach (GameObject obj in _selected)
        {
            obj.TryGetComponent(out Instrument inst);
            if (inst != null)
                inst.selector.SetActive(false);
            else
            {
                obj.GetComponent<ClassicalThread>().ChangeColorBack();
            }
        }
        _selected.Clear();
    }

    public bool IsSelected(GameObject gameObject)
    {
        return (_selected.Contains(gameObject));
    }

    public void AddToSelection(GameObject gameObject)
    {
        _selected.Add(gameObject);
    }

    public void RemoveFromSelection(GameObject gameObject)
    {
        _selected.Remove(gameObject);
    }

    public void SetGridHandler(GridHandler gridHandler)
    {
        _gridHandler = gridHandler;
    }

    public void SetDialogueManager(IDialogueManager manager)
    {
        _dialogueManager = manager;
    }

    public void SetDisplayerManager(IDisplayerManager manager)
    {
        _displayManager = manager;
    }

    public void SetHistoryManager(IHistoryManager manager)
    {
        _historyManager = manager;
    }
}
