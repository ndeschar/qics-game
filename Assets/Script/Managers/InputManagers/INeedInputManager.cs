using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface INeedInputManager
{
    public void SetInputManager(IInputManager manager);
}
