using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class CreditScene : MonoBehaviour
{
    private GameManager gameManager;

    protected void Awake()
    {
        gameManager = GameManager.Instance;
    }

    public void GoBackToMenu()
    {
        if (gameManager == null)
            gameManager = GameManager.Instance;
        gameManager.LoadMenuScene();
    }
}
