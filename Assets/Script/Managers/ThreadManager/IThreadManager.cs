using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IThreadManager : INeedGridHandler
{

    public void Init();
    public ClassicalThread InstantiateThread(Vector3 position);
    
    public void RemoveThread(ClassicalThread thread);

    public List<ThreadData> GetThreadData();

    public void SetThreads(List<ThreadData> datas);

    public void PlaceThread(Connector connectorOne, Connector connectorTwo, bool isNot);

    public void RemoveThread(Connector connectorOne, Connector connectorTwo);

    public void NegateThread(Connector connectorOne, Connector connectorTwo);

    public void ReplaceThreads();
}
