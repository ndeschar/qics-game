using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface INeedThreadManager
{
    public void SetThreadManager(IThreadManager manager);
}
