using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class Book : MonoBehaviour
{
    [SerializeField] Animator _animator;
    [SerializeField] float _fadeDuration = 0.05f;
    private float _fadeVelocity;
    private int _indexOfCurrentPage = 0;
    private int _indexToGoTo;
    private MarquePage[] _marquePages;
    private bool _isAnimating;

    private void Awake()
    {
        _marquePages = GetComponentsInChildren<MarquePage>();
        if (_animator == null)
            _animator = GetComponent<Animator>();
        _fadeVelocity = 1f / _fadeDuration;
    }

    public void OnPageToTurn()
    {
        _indexOfCurrentPage = _indexToGoTo;
        
    }

    public void CloseBook()
    {
        LevelManager.Instance.CloseBook();
    }

    public void TurnPage(MarquePage pageToGoTo)
    {
        if (!_isAnimating)
        {
            _indexToGoTo = System.Array.FindIndex(_marquePages, page => page == pageToGoTo);

            if (_indexToGoTo == _indexOfCurrentPage)
                return;
            
            _isAnimating = true;
            StartCoroutine(FadeOut());


        }
    } 

    public void OnPageTurned()
    {
        if (_isAnimating)
        {
            _marquePages[_indexToGoTo].ActivateComplement();
            _isAnimating = false;
            StartCoroutine(FadeIn());
        }
        
    }

    private void TriggerAnimation()
    {
        if (_indexToGoTo > _indexOfCurrentPage)
        {
            _animator.SetTrigger("TurningRight");
        }
        else if (_indexToGoTo < _indexOfCurrentPage)
        {
            _animator.SetTrigger("TurningLeft");
        }
    }

    private IEnumerator FadeOut()
    {
        float currentAlpha = 1f;
        CanvasGroup canvasToFade = _marquePages[_indexOfCurrentPage].CanvasGroup;
        UnityEngine.UI.Image imageToFade = _marquePages[_indexOfCurrentPage].ComplementImage;

        while (currentAlpha > 0f)
        {
            currentAlpha = currentAlpha - Time.deltaTime * _fadeVelocity;
            if (currentAlpha < 0f)
                currentAlpha = 0f;
            canvasToFade.alpha = currentAlpha;
            imageToFade.color = new Color(imageToFade.color.r, imageToFade.color.b, imageToFade.color.g, currentAlpha);
            yield return new WaitForEndOfFrame();
        }
        canvasToFade.gameObject.SetActive(false);
        TriggerAnimation();
    }

    private IEnumerator FadeIn()
    {
        float currentAlpha = 0f;
        CanvasGroup canvasToFade = _marquePages[_indexOfCurrentPage].CanvasGroup;
        UnityEngine.UI.Image imageToFade = _marquePages[_indexOfCurrentPage].ComplementImage;
        canvasToFade.gameObject.SetActive(true);
        while (currentAlpha < 1f)
        {
            currentAlpha = currentAlpha + Time.deltaTime * _fadeVelocity;
            if (currentAlpha > 1f)
                currentAlpha = 1f;
            canvasToFade.alpha = currentAlpha;
            imageToFade.color = new Color(imageToFade.color.r, imageToFade.color.b, imageToFade.color.g, currentAlpha);
            yield return new WaitForEndOfFrame();
        }

        TriggerAnimation();
    }

}
