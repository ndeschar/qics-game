using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ThemeSpatialMode : ScriptableObject
{
    public Material[] materials = new Material[12];
}
