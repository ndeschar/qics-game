using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ILevelDataManager : INeedThreadManager, INeedGridHandler
{
    public void Init();

    public LevelData GetLevelData();

    public void SetLevel(LevelData data);

    public int[] SetWin(int numberOfCycle);
}
