﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
//using UnityEngine.UIElements;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem.XR.Haptics;

/// <summary>
/// Handle everything related to the game data
/// </summary>
public class GameManager : Singleton<GameManager>
{

    /// <value>
    /// Contains the players datas
    /// </value>
    public UserData userData = null;

    [SerializeField] ThemeSpatialMode _currentThemeSpatialMode;
    [SerializeField] Texture2D _cursorTexture;
    public ThemeSpatialMode CurrentThemeSpatialMode { get { return (_currentThemeSpatialMode); } set { _currentThemeSpatialMode = value; } }

    private Language _currentLanguage = Language.French;
    public Language CurrentLanguage 
    {
        get { return (_currentLanguage); }
        set 
        {
            _currentLanguage = value;
            TranslateEvent?.Invoke();
        } 
    }
    public Dictionary<string, List<string>> levelPrerequisites = new Dictionary<string, List<string>>()
    {
        {"Level1", new List<string>()},
        {"Rotation", new List<string>() {"Level1" } },

        {"Level2", new List<string>() {"Level1" } },

        {"Level3", new List<string>() {"Level2" } },

        {"CollisionUp", new List<string>() {"Level3" }},
        {"ConfigIntro", new List<string>() {"Level3" }},

        {"Configuration", new List<string>() {"ConfigIntro"}},
        {"Superposition", new List<string>() {"ConfigIntro" }},

        {"ThreadIntro", new List<string>() {"Superposition" }},
        {"FuseLane", new List<string>() {"ThreadIntro" }},
        {"ThreadSpawner", new List<string>() {"FuseLane" }},
        {"DefuseLane", new List<string>() {"ThreadSpawner" }},
        {"ThreadOneTwoSwitch", new List<string>() {"DefuseLane" }},


        {"FauxtonDifferentPlaces", new List<string>() {"DefuseLane" }},
        {"ThreeToOne", new List<string>() {"FauxtonDifferentPlaces" }},
         {"ThreeAndThree", new List<string>() {"ThreeToOne" }},
         {"GetTheBlue", new List<string>() {"ThreeToOne" }},
         {"GetTheBlueAgain", new List<string>() {"GetTheBlue" }},

        {"TwoToTwo", new List<string>() {"GetTheBlue" }},
        {"PhaserFirst", new List<string>() {"TwoToTwo" }},
        {"GlobalPhase", new List<string>() {"PhaserFirst" }},
        {"PhaserInstrument", new List<string>() {"GlobalPhase" }},
        {"BSHalf", new List<string>() {"PhaserInstrument" }},
        {"BSHalfNeg", new List<string>() {"BSHalf" }},
        {"Sep2First", new List<string>() {"BSHalfNeg" }},
        {"Sep2", new List<string>() {"Sep2First" }},
        {"Fusing", new List<string>() {"Sep2" }},
        {"Self-Interfere", new List<string>() {"Fusing" }},
        {"Mach-Zehnder", new List<string>() {"Self-Interfere" }},


        {"Level1000", new List<string>() { "Mach-Zehnder" } },  // Example: level 1000 is the first non linear level. It's prerequisite is level 5.
    };
    public bool DEBUG_cheatMode = false;

    public delegate void TranslationTextEvent();
    public event TranslationTextEvent TranslateEvent;

    /// <value>
    /// Name of the file where datas are saved
    /// </value>
    public string nameFile { get; } = "/game.dat";
    private Dictionary<string, GameObject> gameObjectFound = new Dictionary<string, GameObject>();

    private void OnEnable()
    {
        Debug.Log(Application.persistentDataPath);
        //UnityEngine.Cursor.visible = false;
        if (_cursorTexture)
            UnityEngine.Cursor.SetCursor(_cursorTexture, new Vector2(0f, 0f), CursorMode.Auto);
        UnityEngine.Cursor.visible = true;
        Application.targetFrameRate = 60;
        if (File.Exists(Application.persistentDataPath + nameFile))
            LoadGame();
        else
            InitGame();
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    #region Basic Game Handling Features (Saving, Loading, Init ...)
    public void InitGame()
    {
        userData = new UserData();
    }

    void SaveGame()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file;
        file = File.Create(Application.persistentDataPath + nameFile);
        try
        {
            bf.Serialize(file, userData);
        }
        catch (System.Runtime.Serialization.SerializationException e)
        {
            Debug.LogError("Serialization failed");
            Debug.LogError("Failed to serialize. Reason: " + e.Message);
            file.Close();
            throw;
        }
        file.Close();
    }



    public void LoadGame()
    {
        BinaryFormatter bf = new BinaryFormatter();
        if (File.Exists(Application.persistentDataPath + nameFile))
        {
            FileStream file = File.Open(Application.persistentDataPath + nameFile, FileMode.Open, FileAccess.Read);
            try
            {
                userData = (UserData)bf.Deserialize(file);
            }
            catch (System.Runtime.Serialization.SerializationException e)
            {
                Debug.LogError("Deserialization failed");
                Debug.LogError("Failed to deserialize. Reason: " + e.Message);
                file.Close();
                InitGame();
                throw;
            }
            file.Close();
        }

    }

    private void OnApplicationQuit()
    {
        LevelManager levelManager = FindObjectOfType<LevelManager>();
        if (levelManager != null)
            levelManager.SaveLevel();
        SaveGame();
    }

    private void OnApplicationFocus(bool hasFocus)
    {
        if (!hasFocus)
        {
            LevelManager levelManager = FindObjectOfType<LevelManager>();
            if (levelManager != null)
                levelManager.SaveLevel();
            SaveGame();
        }

    }

    #endregion

    #region Level Handling

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        LevelManager levelManager = FindObjectOfType<LevelManager>();
        if (levelManager != null)
            levelManager.SaveLevel();
        SaveGame();
    }

    public void ResetSave()
    {
        if (File.Exists(Application.persistentDataPath + nameFile))
        {
            BinaryFormatter bf = new BinaryFormatter();
            if (File.Exists(Application.persistentDataPath + nameFile))
            {
                File.Delete(Application.persistentDataPath + nameFile);
            }
        }
        InitGame();
    }

    public void LoadLevelScene()
    {
        SceneManager.LoadScene("MenuLevels");
    }

    public void LoadMenuScene()
    {
        SceneManager.LoadScene("MenuScene");
    }

    #endregion

    #region Utility
    public GameObject Find(string name)
    {
        GameObject gameObject;

        if (gameObjectFound.TryGetValue(name, out gameObject) && gameObject != null)
            return (gameObject);
        gameObjectFound[name] = GameObject.Find(name);
        return (gameObjectFound[name]);
    }

    public bool TryToFind(string name, out GameObject outObject)
    {
        GameObject gameObject;

        if (gameObjectFound.TryGetValue(name, out outObject) && outObject != null)
            return (true);
        gameObject = GameObject.Find(name);
        if (gameObject != null)
        {
            gameObjectFound[name] = gameObject;
            outObject = gameObject;
            return (true);
        }
        outObject = null;
        return (false);
    }


    #endregion

}

/// <summary>
/// Language Enum, be careful the name of the toggles for changing language have to be the same name as the one from the enum
/// </summary>
public enum Language
{
    French,
    English
}