using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface INeedDialogueManager
{
    public void SetDialogueManager(IDialogueManager manager);
}
