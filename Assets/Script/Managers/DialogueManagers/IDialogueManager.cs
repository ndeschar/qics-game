using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public interface IDialogueManager : INeedGridHandler
{
    public void LaunchDialogue();
    public void SetFont(TMP_FontAsset font);
    public void SetSizeFont(float size);
    public void SetColorFont(Color color);

    public void Skip();
    public void OnClick();
    public void LoadDialogs();
    public void LoadPanels();
}
