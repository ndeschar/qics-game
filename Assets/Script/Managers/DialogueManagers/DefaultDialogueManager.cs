using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.UI;
using UnityEngine.UIElements;
//using NUnit.Framework;

public enum Emotion
{
    Casual,
    HappyTalking,
    Confused,
    Pensive,
    Explaining,
    Irritated
}

[Serializable]
public struct EmotionsSprite
{
    public Emotion Emotion;
    public Sprite emotionSprite;
}

public class DefaultDialogueManager : MonoBehaviour, IDialogueManager
{
    [SerializeField] float _fontSize = 36f;
    [SerializeField] TMP_FontAsset _fontAsset;
    [SerializeField] float _minTimePerCharacter = 0.005f;
    [SerializeField] UnityEngine.UI.Image _leftProta = null;
    [SerializeField] UnityEngine.UI.Image _rightProta = null;
    [SerializeField] GameObject _textBox = null;
    [SerializeField] TMP_Text _text = null;
    [SerializeField] List<Dialogue> _dialogueList = null;
    [SerializeField] List<UIPanel> _uiPanelList = null;

    private Language _currentLanguage;
    private float _timePerCharacter = 0.05f;
    private Dialogue _currentDialogue = null;
    private UIPanel _currentPanel = null;
    private bool _isPlaying = false;
    private bool _isDialogue = true;
    private Queue<Dialogue> _dialogueQueue;
    private Queue<UIPanel> _uiPanelQueue;
    private List<EmotionsSprite> _leftEmotions;
    private List<EmotionsSprite> _rightEmotions;
    private LevelManager _levelManager;
    private GameManager _gameManager;

    private GridHandler _gridHandler;
    public GridHandler GridHandler
    {
        get
        {
            if (!_gridHandler)
                _levelManager.QueryGridHandler(this);
            return (_gridHandler);
        }

    }

    void Awake()
    {
        _levelManager = LevelManager.Instance;
        _gameManager = GameManager.Instance;
        if (_leftProta == null || _rightProta == null || _textBox == null || _text == null)
        {
            Debug.Log("Missing dialogue element(s)");
            return;
        }
        else if (_minTimePerCharacter <= 0.001)
        {
            Debug.Log("Invalid minTimePerCharacter");
            return;
        }
        _leftEmotions = _levelManager.GetPlayerEmotions();
        _rightEmotions = _levelManager.GetProfEmotions();
        _text.font = _fontAsset;
        _text.fontSize = _fontSize;
        _text.enableWordWrapping = true;
        _text.maxVisibleCharacters = 0;

    }

    public void LaunchDialogue()
    {
        _currentLanguage = _gameManager.CurrentLanguage;
        _dialogueQueue = new Queue<Dialogue>(_dialogueList);
        _uiPanelQueue = new Queue<UIPanel>(_uiPanelList);
        _textBox.SetActive(true);
        OnClick();
    }

    public void LoadDialogs()
    {
        _currentLanguage = _gameManager.CurrentLanguage;
        _isDialogue = true;
        _dialogueQueue = new Queue<Dialogue>(_dialogueList);
        _textBox.SetActive(true);
        _text.maxVisibleCharacters = 0;
        _text.text = "";
        StartCoroutine(Reveal(0));
        OnClick();
    }

    public void LoadPanels()
    {
        _currentLanguage = _gameManager.CurrentLanguage;
        _isDialogue = false;
        _uiPanelQueue = new Queue<UIPanel>(_uiPanelList);
        OnClick();
    }

    public void OnClick()
    {
        if (_isDialogue)
        {
            if (_isPlaying)
            {
                _timePerCharacter = 0;
                //Dialogue instant
            }
            else
            {

                if (_dialogueQueue.Count == 0)
                {
                    _textBox.SetActive(false);
                    _leftProta.gameObject.SetActive(false);
                    _rightProta.gameObject.SetActive(false);
                    _isDialogue = false;
                }
                else
                {
                    _currentDialogue = _dialogueQueue.Dequeue();
                    _currentDialogue.PausePerLetter = _minTimePerCharacter;
                    if (_currentDialogue.FontSize != -1)
                    {
                        _text.fontSize = _currentDialogue.FontSize;
                    }
                    else
                    {
                        _text.fontSize = _fontSize;
                    }
                    AddDialog(_currentDialogue);
                }

            }
        }
        if (!_isDialogue)
        {
            if (_currentPanel != null)
            {
                _currentPanel.Panel.SetActive(false);
            }
                
            if (_uiPanelQueue == null || _uiPanelQueue.Count == 0)
                _levelManager.EndDialogue();
            else
            {
                _currentPanel = _uiPanelQueue.Dequeue();

                GameObject newPanelObj = _currentPanel.Panel;
                newPanelObj.SetActive(true);
                if (_currentPanel.PointSomewhere)
                {
                    newPanelObj.transform.position = GridHandler.GridCoordToWorld(_currentPanel.PointedPosition) + GridHandler.GetGridSize() * Vector3.up * 0.5f;
                }
            }
        }
    }

    public void Skip()
    {
        if (_isDialogue)
        {
            _dialogueQueue.Clear();
            _textBox.SetActive(false);
            _leftProta.gameObject.SetActive(false);
            _rightProta.gameObject.SetActive(false);
            _isDialogue = false;
            if (_currentPanel != null)
            {
                _currentPanel.Panel.SetActive(false);
            }
            if (_uiPanelQueue == null || _uiPanelQueue.Count == 0)
                _levelManager.EndDialogue();
            else
            {
                _currentPanel = _uiPanelQueue.Dequeue();

                GameObject newPanelObj = _currentPanel.Panel;
                newPanelObj.SetActive(true);
                if (_currentPanel.PointSomewhere)
                {
                    newPanelObj.transform.position = GridHandler.GridCoordToWorld(_currentPanel.PointedPosition) + GridHandler.GetGridSize() * Vector3.up * 0.5f;
                }
            }

        }
        else
        {
            if (_currentPanel != null)
                _currentPanel.Panel.SetActive(false);
            _currentPanel = null;
            _uiPanelQueue.Clear();
            _levelManager.EndDialogue();
        }
    }



    public IEnumerator Reveal(int startLetterIndex)
    {
        _text.ForceMeshUpdate();

        int totalVisibleCharacters = _text.textInfo.characterCount;

        _isPlaying = true;
        for (int i = startLetterIndex; i < totalVisibleCharacters; i++)
        {
            if (_timePerCharacter == 0)
            {
                break;
            }
            _text.maxVisibleCharacters = i + 1;

            yield return new WaitForSeconds(_timePerCharacter);
        }

        _text.maxVisibleCharacters = totalVisibleCharacters;
        _isPlaying = false;
        yield break;
    }

    internal void AddDialog(Dialogue message)
    {
        _timePerCharacter = message.PausePerLetter;
        string quote = message.ChooseQuote(_currentLanguage);


        if (message.CharacId == 0)
        {
            _leftProta.gameObject.SetActive(true);
            _leftProta.sprite = _leftEmotions.Find((currentEmotion) => (currentEmotion.Emotion == message.CharacterEmotion)).emotionSprite;
            _rightProta.gameObject.SetActive(false);
        }
        else if (message.CharacId == 1)
        {
            _leftProta.gameObject.SetActive(false);
            _rightProta.gameObject.SetActive(true);
            _rightProta.sprite = _rightEmotions.Find((currentEmotion) => (currentEmotion.Emotion == message.CharacterEmotion)).emotionSprite;
        }


        if (message.NewPage)
        {
            _text.maxVisibleCharacters = 0;
            _text.text = quote;
            StartCoroutine(Reveal(0));
        }
        else
        {
            int currentLetterIndex = _text.maxVisibleCharacters;

            if (message.NewLine)
            {
                _text.text = string.Concat(_text.text, "\n", quote);
            }
            else
            {
                _text.text = string.Concat(_text.text, " ", quote);
            }

            StartCoroutine(Reveal(currentLetterIndex));
        }
    }


    public void SetGridHandler(GridHandler gridHandler)
    {
        _gridHandler = gridHandler;
    }

    public void SetFont(TMP_FontAsset font)
    {
        throw new NotImplementedException();
    }
    public void SetSizeFont(float size)
    {
        throw new NotImplementedException();
    }
    public void SetColorFont(Color color)
    {
        throw new NotImplementedException();
    }

}



[Serializable]
internal class UIPanel
{
    [SerializeField] GameObject _panel;
    [SerializeField] bool _pointSomewhereOnGrid;
    [SerializeField] Vector2Int _pointedPosition;

    public bool PointSomewhere => _pointSomewhereOnGrid;
    public Vector2Int PointedPosition => _pointedPosition;
    public GameObject Panel => _panel;

}

[Serializable]
internal class Dialogue
{
    [TextArea(5, 20), SerializeField] string _quote = "";
    [TextArea(5, 20), SerializeField] string _quoteEnglish = "";
    [SerializeField] float _pausePerLetter = 0.1f;
    [SerializeField] int _fontSize = -1;
    [SerializeField] bool _newPage;
    [SerializeField] bool _newLine;
    [SerializeField] Emotion _characterEmotion;
    [SerializeField] int _characId;

    public float PausePerLetter
    {
        get { return (_pausePerLetter); }
        set { _pausePerLetter = value; }
    }

    public bool NewPage => _newPage;

    public bool NewLine => _newLine;

    public int CharacId => _characId;

    public int FontSize => _fontSize;

    public Emotion CharacterEmotion => _characterEmotion;

    public PropertyName id => new PropertyName();


    public string ChooseQuote(Language language) => language switch
    {
        Language.French => _quote,
        Language.English => _quoteEnglish,
        _ => throw new ArgumentOutOfRangeException(nameof(language), $"Unknown language: {language}"),
    };

}
